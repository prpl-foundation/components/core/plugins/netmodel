/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef __NETMODEL_ARG_H__
#define __NETMODEL_ARG_H__
/**
   @defgroup arg arg.h - Argument parsing
   @{
 */
#include <stdbool.h>

#include "intf.h"
#include "traverse.h"

/**
   @brief
   Translate a string argument holding a written flagset into a flagset.

   @param set The set into which the written flagset will be parsed.
   @param args The argument list to search.
   @param name Name of the string argument to parse.
   @param status If parsing fails, an error code will be set in this variable. This argument may be NULL.
   @return True if the argument could be parsed successfully. False if parsing the argument failed (i.e. syntax error).
 */
bool arg_get_set(amxc_set_t* set,
                 const amxc_var_t* args,
                 const char* name,
                 amxd_status_t* status);
/**
   @brief
   Translate a string argument holding a written flag expression into a flag expression.

   @param expr The expression into which the written flag expression will be parsed.
   @param args The argument list to search.
   @param name Name of the string argument to parse.
   @param status If parsing fails, an error code will be set in this variable. This argument may be NULL.
   @return True if the argument could be parsed successfully. False if parsing the argument failed (i.e. syntax error).
 */
bool arg_get_expr(amxp_expr_t* expr,
                  const amxc_var_t* args,
                  const char* name,
                  amxd_status_t* status);
/**
   @brief
   Translate a string argument holding a traverse mode into a traverse mode enumeration value.

   @param mode The traverse mode enum which will be set.
   @param args The argument list to search.
   @param name Name of the string argument to parse.
   @param status If parsing fails, an error code will be set in this variable. This argument may be NULL.
   @return True if the argument could be parsed successfully. False if parsing the argument failed (i.e. invalid traverse mode).
 */
bool arg_get_traverse(traverse_mode_t* mode,
                      const amxc_var_t* args,
                      const char* name,
                      amxd_status_t* status);

/**
   @brief
   Translate a string argument holding an interface name into an Intf.

   @param intf The target to store the Intf into.
   @param args The argument list to search.
   @param name Name of the string argument to parse.
   @param status If parsing fails, an error code will be set in this variable. This argument may be NULL.
   @return True if the argument could be parsed successfully. False if parsing the argument failed (i.e. Intf does not exist).
 */
bool arg_get_intf(intf_t** intf,
                  const amxc_var_t* args,
                  const char* name,
                  amxd_status_t* status);

/**
   @brief
   Get a string argument from the argument list.

   @param target The target to store the string into.
   @param args The argument list to search.
   @param name Name of the string argument to find.
   @param status If parsing fails, an error code will be set in this variable. This argument may be NULL.
   @return True if the argument could be found.
 */
bool arg_get_string(const char** target,
                    const amxc_var_t* args,
                    const char* name,
                    amxd_status_t* status);
/**
   @brief
   Get an uint16 argument from the argument list.

   @param target The target to store the string into.
   @param args The argument list to search.
   @param name Name of the uint16 argument to find.
   @param status If parsing fails, an error code will be set in this variable. This argument may be NULL.
   @return True if the argument could be found.
 */
bool arg_get_uint16(uint16_t* target,
                    const amxc_var_t* args,
                    const char* name,
                    amxd_status_t* status);

/**
   @brief
   Get a variant argument from the argument list.

   @param target The target to store the variant.
   @param args The argument list to search.
   @param name Name of the argument to find.
   @param status If parsing fails, an error code will be set in this variable. This argument may be NULL.
   @return True if the argument could be found.
 */
bool arg_get_var(amxc_var_t* target,
                 const amxc_var_t* args,
                 const char* name,
                 amxd_status_t* status);

/**
   @}
 */

#endif // __NETMODEL_ARG_H__
