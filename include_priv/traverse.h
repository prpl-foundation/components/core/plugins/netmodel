/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef __NETMODEL_TRAVERSE_H__
#define __NETMODEL_TRAVERSE_H__

/**
   @defgroup traverse traverse.h - Traverse tree management
   @{
 */

#include "intf.h"

/**
   @brief
   Enumeration of traverse modes.
 */
typedef enum _traverse_mode {
    traverse_mode_this,           /**< @brief Consider only the starting Intf.*/
    traverse_mode_down,           /**< @brief Consider the entire closure formed by recursively following the LLIntf references.*/
    traverse_mode_up,             /**< @brief Consider the entire closure formed by recursively following the ULIntf references.*/
    traverse_mode_down_exclusive, /**< @brief The same as traverse_mode_down, but exclude the starting Intf.*/
    traverse_mode_up_exclusive,   /**< @brief The same as traverse_mode_up, but exclude the starting Intf.*/
    traverse_mode_one_level_down, /**< @brief Consider only direct LLIntfs.*/
    traverse_mode_one_level_up,   /**< @brief Consider only direct ULIntfs.*/
    traverse_mode_all,            /**< @brief Consider all Intfs.*/
} traverse_mode_t;

typedef struct _traverse_node traverse_tree_t; /**< @brief Opaque object holding a traverse tree. */
typedef struct _traverse_node traverse_node_t; /**< @brief Opaque handle to represent a node of a traverse tree. */
typedef struct _traverse_edge traverse_edge_t; /**< @brief Opaque handle to represent an edge of a traverse tree. */

/**
   @brief
   Convert a text based traverse mode to an enumeration based traverse mode.

   @details
   This function can only return successfully if the provided text based traverse mode equals one of the predefined traverse mode
   literals. If this is not the case, an error message is logged.

   @param mode The target to store the enumeration based traverse mode into. It's left untouched if it is NULL or if the conversion
            failed.
   @param str The string holding the text based traverse mode.
   @return True if the string could be converted successfully.
 */
bool traverse_mode_parse(traverse_mode_t* mode, const char* str);

/**
   @brief
   Convert an enumeration based traverse mode to a text based traverse mode.

   @param mode The enumeration based traverse mode
   @return A string holding the text based traverse mode.
 */
const char* traverse_mode_string(traverse_mode_t mode);

/**
   @brief
   Get the Intf that is represented by a node of the traverse tree.

   @param node The node to get the Intf from.
   @return The Intf that is represented by the node.
 */
intf_t* traverse_node_intf(traverse_node_t* node);

/**
   @brief
   Start iterating over the edges starting at a node of the traverse tree.

   @details
   "Starting" in this context means that the edges to child nodes are iterated, not edges to parent nodes.

   @param node The node to start iterating the edges from.
   @return The first edge starting at the node.
 */
traverse_edge_t* traverse_node_first_edge(traverse_node_t* node);

/**
   @brief
   Mark a node of the traverse tree with an unsigned long marker.

   @details
   This is very useful because nodes may be referred to by multiple edges, so when traversing the traverse tree, it is possible
   that a single node may be traversed multiple times. This marker allows callers to detect this, by setting a flag for each visited
   node or to store a previously computed result in the marker so it can be reused when the node is visited multiple times.

   @param node The node to mark.
   @param marker The marker to set.
 */
void traverse_node_mark(traverse_node_t* node, unsigned long marker);

/**
   @brief
   Get the marker of a node of the traverse tree.

   @param node The node to get the marker from.
   @return The marker of the node, set with traverse_node_mark().
 */
unsigned long traverse_node_marker(traverse_node_t* node);

/**
   @brief
   Find a node in the traverse tree for a given intf.

   @param node The root node to start searching.
   @param intf The intf to look for.
   @return The node that represents the given intf in the traverse tree if such node was found, NULL otherwise.
 */
traverse_node_t* traverse_node_find(traverse_node_t* node, intf_t* intf);

/**
   @brief
   Iterate over the edges starting at a node of the traverse tree.

   @param edge The current edge.
   @return The next edge.
 */
traverse_edge_t* traverse_edge_next(traverse_edge_t* edge);

/**
   @brief
   Get the node of the traverse tree referenced by the current edge.

   @param edge The current edge.
   @return The node referenced by the current edge.
 */
traverse_node_t* traverse_edge_node(traverse_edge_t* edge);

/**
   @brief
   Create a traverse tree by applying a given traverse mode to a given Intf.

   @param mode The traverse mode to apply.
   @param intf The Intf to start from.
   @return The resulting traverse tree. Should be released again with traverse_tree_destroy().
 */
traverse_tree_t* traverse_tree_create(traverse_mode_t mode, intf_t* intf);

/**
   @brief
   Traverse tree desctructor.

   @param tree The traverse tree to destroy. When this function returns, this pointer is no longer valid. The same for all pointers
            to nodes and edges referenced by this traverse tree.
 */
void traverse_tree_destroy(traverse_tree_t* tree);

/**
   @brief
   Get the root node of a traverse tree.

   @param tree The traverse tree to get the root node of.
   @return The traverse tree's root node.
 */
traverse_node_t* traverse_tree_root(traverse_tree_t* tree);

/**
   @brief
   Reset all markers of a traverse tree to 0 at once.

   @param tree The traverse tree of which all markers should be cleared.
 */
void traverse_tree_clear_marks(traverse_tree_t* tree);

/**
   @brief
   Call a provided callback function once for each Intf occurring in a traverse tree.

   @param tree The traverse tree to be traversed.
   @param step Callback function to call for each Intf. This function should return true to stop the traversal immediately and
            false to continue.
   @param userdata Void pointer to pass to the callback function.
   @return True if the traversal was terminated by the callback function returning true, false if the callback function returned false
        for every single Intf occurring in the traverse tree.

   @note
   The callback function is called exactly once for each Intf occurring in the traverse tree, even if there are multiple paths to
   reach the same Intf. This function uses the traverse node markers in order to achieve this behaviour, so please don't touch them
   in the callback function.

   @note
   This function walks through Intfs in a specific order and callers are allowed to count on this.
   - Siblings in the tree occur in the order the corresponding ULIntf->LLIntf dependencies are created,
   the Intf that was linked first with the parent will be traversed first.
   - This function applies a simple depth-first search. So the children of the n'th sibling are traversed before the (n+1)'th sibling.
   .
   And moreover, callers may safely assume that Intf A is traversed before Intf B if Intf B is not reachable from the root without
   passing Intf A first.
   \n This property holds for all data model / common API functions that use this method too. This includes all functions with query
   support except netmodel_isUp().
 */
bool traverse_tree_walk(traverse_tree_t* tree,
                        bool (* step)(intf_t* intf, void* userdata),
                        void* userdata);

/**
   @brief
   Convert a traverse tree into written form.

   @details
   This is a feature intended for debugging.

   @param tree The traverse tree to convert.
   @return A string holding the written traverse tree. It's allocated on the heap and needs to be freed by the caller with
        free(). A NULL pointer is returned if tree is NULL or if memory allocation failed.
 */
char* traverse_tree_print(traverse_tree_t* tree);

/**
   @}
 */

#endif

