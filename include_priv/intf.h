/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef __NETMODEL_INTF_H__
#define __NETMODEL_INTF_H__

/**
   @defgroup intf intf.h - Intf management
   @{
 */

#include <stdbool.h>

#include "netmodel.h"

typedef struct _intf intf_t;           /**< @brief Internal data type used to reference an Intf. */
typedef struct _intf_link intf_link_t; /**< @brief Handle for iterating over LLIntf/ULIntf dependencies. */


/**
   @brief
   Callback function type for link or dependency event listeners.

   @details
   A link event listener is called whenever a ULIntf->LLIntf dependency is created or removed.

   @param ulintf The ULIntf of the created or removed link.
   @param llintf The LLIntf of the created or removed link.
   @param value True if the link was created, false if it was removed.
   @param userdata Void pointer passed to intf_addLinkListener().

   @remark
   The handler is called right AFTER the link is created or removed.

   @remark
   When an Intf is destroyed, its ULIntf and LLIntfs are removed first, implying the invokation of the event listeners. This means
   that it is safe to keep a reference to an Intf between getting notified of a specific link being created and that same link being
   removed again.

   @see intf_addLinkListener()
 */
typedef void (* intf_linklistener)(intf_t* ulintf, intf_t* llintf, bool value, void* userdata);

/**
   @brief
   Intf constructor.

   @details
   A corresponding Intf object in the data model will be created automatically before this function returns.
   By default, an Intf contains no flags and no lower layer or upper layer dependencies.

   @param name The name of the new Intf
   @return The newly created Intf.
 */
intf_t* intf_create(const char* name);

/**
   @brief
   Intf destructor.

   @details
   This function clears the Intf's flagset and its lower layer and upper layer dependencies first, implying the invokation of
   corresponding event listeners.
   The corresponding Intf object in the data model will be deleted automatically before this function returns.

   @param intf The Intf to destroy. When this function returns, this pointer is no longer valid.
 */
void intf_destroy(intf_t* intf);

/**
   @brief
   Find an Intf given its name.

   @param name The name of the Intf to find.
   @return The Intf if it was found, NULL otherwise.
 */
intf_t* intf_find(const char* name);

/**
   @brief
   Start iterating over the global list of Intfs.

   @return The first Intf in the list.
 */
intf_t* intf_first(void);

/**
   @brief
   Iterate over the global list of Intfs.

   @param intf The current Intf.
   @return The next Intf in the list.
 */
intf_t* intf_next(intf_t* intf);

/**
   @brief
   Get an Intf's flag set.

   @param intf The Intf to get the flag set of.
   @return Not a copy, but the Intf's internal flag set, (must not be destroyed by the caller!) so it can be used to both query and
        manipulate the Intf's flag set. When manipulating the flag set, the Intf will notice through its alert handler (please do
        not overwrite this!) and it will invoke all Intf flag listeners on its turn.
 */
amxc_set_t* intf_flagset(intf_t* intf);

/**
   @brief
   Get an Intf's name.

   @param intf The Intf to get the name of.
   @return The Intf's name.
 */
const char* intf_name(intf_t* intf);

/**
   @brief
   Start iterating over the Intf's LLIntfs.

   @param intf The Intf to get the LLIntfs of.
   @return An iterator pointing to the first LLIntf of the Intf. It can be retrieved with intf_link_current(). If the Intf has no
        LLIntfs, this function returns a NULL pointer.
 */
intf_link_t* intf_llintfs(intf_t* intf);

/**
   @brief
   Start iterating over the Intf's ULIntfs.

   @param intf The Intf to get the ULIntfs of.
   @return An iterator pointing to the first ULIntf of the Intf. It can be retrieved with intf_link_current(). If the Intf has no
        ULIntfs, this function returns a NULL pointer.
 */
intf_link_t* intf_ulintfs(intf_t* intf);

/**
   @brief
   Get the datamodel object associated with this intf.

   @param intf The Intf to get the object of.
   @return A pointer to the object.
 */
amxd_object_t* intf_object(intf_t* intf);

/**
   @brief
   Set a datamodel object to be associated with this intf.

   @param intf The Intf to associate the object with.
   @param object The object to associate the intf with.
 */
void intf_object_set(intf_t* intf, amxd_object_t* object);

/**
   @brief
   Retrieve the Intf that is represented by the current iterator.

   @param link The current LLIntf/ULIntf iterator.
   @return The Intf is represented by the current iterator.
 */
intf_t* intf_link_current(intf_link_t* link);

/**
   @brief
   Iterate over the LLIntfs/ULIntfs.

   @param link The current Intf iterator.
   @return An iterator pointing to the next LLIntf/ULIntf. If the Intf contains no more LLIntfs/ULIntfs, this function returns a
        NULL pointer.
 */
intf_link_t* intf_link_next(intf_link_t* link);

/**
   @brief
   Create an ULIntf->LLIntf dependency if it does not exist already.

   @details
   This function invokes the link event listeners if the link needed to be created.

   @param ulintf The ULIntf of the dependency or the Intf to add the LLIntf dependency to.
   @param llintf The LLIntf of the dependency or the Intf to add the ULIntf dependency to.
 */
void intf_link(intf_t* ulintf, intf_t* llintf);

/**
   @brief
   Remove an ULIntf->LLIntf dependency if it exists.

   @details
   This function invokes the link event listeners if the link existed.

   @param ulintf The ULIntf of the dependency or the Intf to remove the LLIntf dependency from.
   @param llintf The LLIntf of the dependency or the Intf to remove the ULIntf dependency from.
 */
void intf_unlink(intf_t* ulintf, intf_t* llintf);

/**
   @brief
   Register an Intf link event listener.

   @param ulintf If NULL, subscribe for link events for any ULIntf. If non-NULL, subscribe only for link events for which the given
              Intf is the ULIntf.
   @param llintf If NULL, subscribe for link events for any LLIntf. If non-NULL, subscribe only for link events for which the given
              Intf is the LLIntf.
   @param l The callback function to be called whenever a link is created or removed.
   @param userdata A void pointer to pass to the callback function.
   @param boost If set to true, call the callback function with value=true before this function returns for each link for which
             it would have been called if the listener was already registered since eternity (i.e. before the links arose).
 */
void intf_add_linklistener(intf_t* ulintf, intf_t* llintf, intf_linklistener l, void* userdata, bool boost);

/**
   @brief
   Unregister an Intf link event listener.

   @param ulintf Should equal the ulintf argument of the corresponding call to intf_addLinkListener().
   @param llintf Should equal the llintf argument of the corresponding call to intf_addLinkListener().
   @param l Should equal the l argument of the corresponding call to intf_addLinkListener().
   @param userdata Should equal the userdata argument of the corresponding call to intf_addLinkListener().
   @param teardown If set to true, call the callback function with value=false before this function returns for each link for which
                it would have been called if the listener was registered until eternity (i.e. when all links are destroyed).
 */
void intf_del_linklistener(intf_t* ulintf, intf_t* llintf, intf_linklistener l, void* userdata, bool teardown);

/**
   @brief
   Get the list of queries created on this intf.

   @param intf The Intf to get the object of.
   @return A pointer to the list
 */
amxc_llist_t* intf_queries(intf_t* intf);

/**
   @}
 */

#endif
