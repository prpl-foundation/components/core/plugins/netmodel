# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v2.17.15 - 2024-12-10(10:36:36 +0000)

### Other

- [NetModel] getDHCPOption query for RA does not work

## Release v2.17.14 - 2024-12-06(15:06:49 +0000)

### Other

- [NetModel] Further cpu load optimizations

## Release v2.17.13 - 2024-12-03(11:00:33 +0000)

### Other

- - [routeradvertisement] A and L flags must be set by default

## Release v2.17.12 - 2024-11-21(15:18:15 +0000)

### Other

- [NetModel] Link/unlink sequence can result in an endless event toggle

## Release v2.17.11 - 2024-11-18(12:33:58 +0000)

### Other

- [netmodel][dslite] - DSLite status remains in Error after EndpointName is updated

## Release v2.17.10 - 2024-11-08(14:05:55 +0000)

### Other

- Reduce regex usage in signal handling

## Release v2.17.9 - 2024-11-05(15:43:21 +0000)

### Other

- [NetModel] Optimize query retriggering

## Release v2.17.8 - 2024-11-05(08:16:30 +0000)

### Other

- [NetModel] NetModel not starting if pcb bus is not

## Release v2.17.7 - 2024-10-28(16:34:56 +0000)

### Other

- [NetModel] Extend query statistics

## Release v2.17.6 - 2024-10-07(08:51:07 +0000)

### Other

- [NetModel] Sync Status_ext parameter to Status

## Release v2.17.5 - 2024-09-18(10:16:41 +0000)

### Other

- [NetModel] Only sync Status and Enable parameters in one direction

## Release v2.17.4 - 2024-09-18(10:00:25 +0000)

### Other

- [NetModel] Set correct permissions on direct socket

## Release v2.17.3 - 2024-09-05(07:42:32 +0000)

### Other

- [NetModel] Connect via direct socket if possible

## Release v2.17.2 - 2024-07-23(07:35:05 +0000)

### Fixes

- Better shutdown script

## Release v2.17.1 - 2024-07-05(13:08:05 +0000)

### Other

- PRPL/COVERITY:netmodel issue

## Release v2.17.0 - 2024-04-19(10:41:32 +0000)

### New

- - [Cellular] Add support of cellular interfaces

## Release v2.16.0 - 2024-04-11(15:33:19 +0000)

### New

- add moca mib odl file

## Release v2.15.8 - 2024-04-10(10:00:32 +0000)

### Changes

- Make amxb timeouts configurable

## Release v2.15.7 - 2024-03-22(15:46:54 +0000)

### Fixes

- [NetModel] Use strict stringcompare instead of substring matching to resolve paths

## Release v2.15.6 - 2024-03-21(09:10:07 +0000)

### Fixes

- After reboot all hosts are disconnected (AKA amb timeouts)

## Release v2.15.5 - 2024-03-21(09:02:12 +0000)

### Other

- Creating and Adding the wds mib.

## Release v2.15.4 - 2024-03-20(09:21:30 +0000)

### Other

- - add MACAddress and BSSID for ssid mib.

## Release v2.15.3 - 2024-01-16(16:29:45 +0000)

### Fixes

- Load Flags for default interfaces in event handler instead of action handler

## Release v2.15.2 - 2024-01-15(15:03:59 +0000)

### Fixes

- Initialize NetModel private data using an action handler instead of event handler

## Release v2.15.1 - 2023-10-13(14:02:25 +0000)

### Changes

-  All applications using sahtrace logs should use default log levels

## Release v2.15.0 - 2023-10-09(10:50:49 +0000)

### New

- make it possible to query MTU

## Release v2.14.0 - 2023-10-09(09:00:05 +0000)

### New

- [amxrt][no-root-user][capability drop]Netmodel/Netmodel-clients must be adapted to run as non-root and lmited capabilities

## Release v2.13.0 - 2023-10-03(06:57:28 +0000)

### New

- [amx][conmon] ConMon must support arp(ipv4) and icmpv6(ipv6)

## Release v2.12.0 - 2023-09-28(07:06:16 +0000)

### New

- Create a statmon client for netmodel

## Release v2.11.0 - 2023-09-26(11:41:57 +0000)

### New

- [AP Config][IPv4 connectivity] Repeater should switch from static to dhcp ip address

## Release v2.10.4 - 2023-09-22(16:19:40 +0000)

### Fixes

- Correctly clean up object<->intf pointers

## Release v2.10.3 - 2023-09-21(11:25:38 +0000)

### Fixes

- [Fut][Random]Youtube and facebook are not accessible with a browser (firefox) - no RA on LAN

## Release v2.10.2 - 2023-09-07(10:14:51 +0000)

### Fixes

- [NetModel] Fix query triggering when multiple parameters are updated at the same time

## Release v2.10.1 - 2023-07-03(12:42:33 +0000)

### Fixes

- Init script has no shutdown function

## Release v2.10.0 - 2023-05-30(11:51:10 +0000)

### New

- [CDROUTER][IPv6] The Box is still advertising itself as router at reception on WAN of RA with router lifetime 0

## Release v2.9.0 - 2023-05-04(09:40:19 +0000)

### New

- Sync xpon EthernetUNI with NetModel

## Release v2.8.0 - 2023-04-12(12:09:17 +0000)

### New

- Support IPv6 unnumbered mode

## Release v2.7.0 - 2023-04-04(10:42:15 +0000)

### New

- Create an event when a renew is received

## Release v2.6.4 - 2023-03-15(10:43:09 +0000)

### Fixes

- Still invalid prefix in the Prefixes parameter

## Release v2.6.3 - 2023-03-09(09:17:52 +0000)

### Other

- [Config] enable configurable coredump generation

## Release v2.6.2 - 2023-02-28(18:11:16 +0000)

### Fixes

- Return the relative lifetimes in getIPv6Prefix result

## Release v2.6.1 - 2023-02-24(08:54:04 +0000)

### Fixes

- Return index in getIPv6Prefix

## Release v2.6.0 - 2023-02-23(11:41:12 +0000)

### New

- Box sends 2 ICMPv6 RA when a RS is received on LAN

## Release v2.5.4 - 2023-01-12(10:57:21 +0000)

### Fixes

- [NetModel] Check query flag argument using regex when retriggering

## Release v2.5.3 - 2022-12-09(09:18:07 +0000)

### Fixes

- [Config] coredump generation should be configurable

## Release v2.5.2 - 2022-12-05(12:58:57 +0000)

### Fixes

- [netmodel][DHCPv6Client][getDHCPOption] DHCPv6 option 24 is wrongly parsed

## Release v2.5.1 - 2022-11-25(11:29:00 +0000)

### Other

- [NetModel] Update copyright information

## Release v2.5.0 - 2022-11-17(07:09:17 +0000)

### New

- [dslite][netmodel] Create a netmodel client for dslite interfaces.

## Release v2.4.2 - 2022-11-15(12:38:13 +0000)

### Other

- [NetModel] Reduce the amount of variant copies

## Release v2.4.1 - 2022-11-07(16:59:34 +0000)

### Changes

- [NetModel] Make the Intf Name parameter writable

## Release v2.4.0 - 2022-10-25(09:49:36 +0000)

### New

- [NetModel] Add statistics for queries and events

## Release v2.3.0 - 2022-10-21(12:28:31 +0000)

### New

- Add API to get all link information

## Release v2.2.4 - 2022-10-21(06:55:05 +0000)

### Fixes

- [NetModel] Take null variants into account when comparing query results

## Release v2.2.3 - 2022-10-19(06:46:53 +0000)

### Other

- [NetModel] Use amxc_var_compare to compare htable and list variants

## Release v2.2.2 - 2022-10-18(14:24:48 +0000)

### Fixes

- Add support of the mib for the brigde tag

## Release v2.2.1 - 2022-10-06(08:40:00 +0000)

### Fixes

- [Netmodel] remove lowerlayers parameter from ssid and radio

## Release v2.2.0 - 2022-08-29(15:11:34 +0000)

### New

- Dynamic handling of network events (PPP)

## Release v2.1.0 - 2022-08-25(08:29:51 +0000)

### New

- [netmodel][client] Add support for radio and ssid clients in netmodel

## Release v2.0.0 - 2022-08-05(09:51:17 +0000)

### Removed

- [NetModel] Remove registerSync API

## Release v1.17.1 - 2022-06-21(08:02:16 +0000)

### Other

- [PRPLoS] Reduce logging during start up.

## Release v1.17.0 - 2022-05-23(11:56:37 +0000)

### New

- [netmodel][client] Add support for ppp clients in netmodel

## Release v1.16.1 - 2022-04-21(15:43:52 +0000)

### Fixes

- NetModel getAddr asynchronous query not working

## Release v1.16.0 - 2022-04-19(12:00:34 +0000)

### New

- [netmodel] Support of prefix queries in netmodel/libnetmodel

## Release v1.15.3 - 2022-04-06(06:21:02 +0000)

### Changes

- [NetModel] Change query updated event to match the event sent by NeMo

## Release v1.15.2 - 2022-04-05(06:43:15 +0000)

### Changes

- The loopback interface must be marked with the netdev flag.

## Release v1.15.1 - 2022-03-24(10:59:18 +0000)

### Changes

- [GetDebugInformation] Add data model debuginfo in component services

## Release v1.15.0 - 2022-03-17(09:04:15 +0000)

### New

- implement iprouter client and iprouter mib

## Release v1.14.3 - 2022-03-14(15:52:10 +0000)

### Other

- [NetModel] Add doxygen generation

## Release v1.14.2 - 2022-02-28(10:47:38 +0000)

### Fixes

- [NetModel] Create separate plugin to run NetModel clients

## Release v1.14.1 - 2022-02-25(10:54:14 +0000)

### Other

- Enable core dumps by default

## Release v1.14.0 - 2022-02-22(13:50:43 +0000)

### New

- [NetModel] Add getMibs API

## Release v1.13.5 - 2022-02-18(14:03:53 +0000)

### Fixes

- [NetModel] Typo in regex for dhcpOption change events

## Release v1.13.4 - 2022-02-16(20:41:52 +0000)

### Fixes

- [NetModel] Crash when opening the same getDHCPOption query twice

## Release v1.13.3 - 2022-02-15(22:26:03 +0000)

### Fixes

- [NetModel] Bridging instances are no longer added to NetModel

## Release v1.13.2 - 2022-02-15(15:51:54 +0000)

### Changes

- [NetModel] Disable datamodel persistency

## Release v1.13.1 - 2022-02-09(07:51:50 +0000)

### Fixes

- [NetModel] Ensure that the private intf_t structure is created when needed

## Release v1.13.0 - 2022-02-08(11:08:34 +0000)

### New

- [netmodel] Support getDHCPOption() queries

## Release v1.12.0 - 2022-02-07(17:00:14 +0000)

### New

- create netmodel client for tr181-logical

## Release v1.11.0 - 2022-02-04(17:23:25 +0000)

### New

- implement netdev client and netdev mib

## Release v1.10.0 - 2022-01-31(15:30:20 +0000)

### New

- Integrate support for bridging in NetModel

## Release v1.9.2 - 2022-01-26(19:50:52 +0000)

### Changes

- Only use index paths when opening NetModel queries

## Release v1.9.1 - 2022-01-25(15:24:10 +0000)

### Fixes

- Prevent amxp_expr_clean of uninitialized expression

## Release v1.9.0 - 2022-01-14(12:54:02 +0000)

### New

- Support queries on TR181 paths that are not yet present

## Release v1.8.0 - 2022-01-14(08:47:18 +0000)

### New

- IP client startup does not always work properly

## Release v1.7.0 - 2022-01-11(10:03:07 +0000)

### New

- Create DHCPv6 Client mapping in NetModel

## Release v1.6.0 - 2022-01-10(12:49:40 +0000)

### New

- Create DHCPv4 Client mapping in NetModel

### Changes

- implement ip client and ip mib

## Release v1.5.0 - 2022-01-04(08:19:46 +0000)

### New

- create mib_ethernet

## Release v1.4.1 - 2021-12-16(15:13:06 +0000)

### Fixes

- implement ip client and ip mib

## Release v1.4.0 - 2021-12-14(17:27:13 +0000)

### New

- Implement IP address queries

## Release v1.3.3 - 2021-12-08(16:12:24 +0000)

### Changes

- [amx][NetModel] Add guest interface to defaults

## Release v1.3.2 - 2021-12-06(14:14:11 +0000)

### Fixes

- [amx][NetModel] Handle events from entry point events before sending app:start

## Release v1.3.1 - 2021-11-15(16:07:30 +0000)

### Other

- Register on start event

## Release v1.3.0 - 2021-10-27(08:40:07 +0000)

### New

- Send events when (un)linking interfaces

### Other

- [BAF] add support for amx docgen
- [BAF] add support for amx docgen

## Release v1.2.1 - 2021-10-23(07:09:33 +0000)

### Changes

- [NetModel] Interface path must start with "Device."

## Release v1.2.0 - 2021-10-06(12:46:41 +0000)

### New

- Create Tr181 Interface clients

## Release v1.1.0 - 2021-09-07(11:36:18 +0000)

### New

- Implement data model functions
- Implement query functionality
- Interface flags implementation

## Release v1.0.2 - 2021-06-23(14:30:00 +0000)

### Fixes

- [Netmodel] Update changelogs

## Release v1.0.1 - 2021-06-23(11:42:46 +0000)

### New

- [Netmodel] Load MIBs based on interface flags
- Add generation of the datamodel documentation

## Release v1.0.0 - 2021-06-18(07:30:26 +0000)

### New

- Initial release
