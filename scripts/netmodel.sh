#!/bin/sh

. /usr/lib/amx/scripts/amx_init_functions.sh

name="netmodel"
datamodel_root="NetModel"

# These variables are used in the NetModel odl file
export own_uid=$(id -u)
export tr181_app_gid=$(id -g tr181_app)

case $1 in
    boot)
        process_boot ${name} -D
        ;;
    start)
        process_start ${name} -D
        ;;
    stop)
        process_stop ${name}
        ;;
    shutdown)
        process_shutdown ${name}
        ;;
    restart)
        $0 stop
        $0 start
        ;;
    debuginfo)
        process_debug_info ${datamodel_root}
        ;;
    *)
        echo "Usage : $0 [start|boot|stop|shutdown|debuginfo|restart]"
        ;;
esac
