/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <debug/sahtrace.h>

#include "intf.h"
#include "arg.h"
#include "dm_intf.h"
#include "traverse.h"

#define ME "netmodel"

typedef enum {
    SET_FLAG,
    CLEAR_FLAG,
    HAS_FLAG
} flag_op_t;

typedef struct _flag_ctx {
    amxc_set_t* flagset;
    amxp_expr_t* condition;
    flag_op_t op;
} flag_ctx_t;

enum node_state {
    NODE_NEW = 0,
    NODE_DOWN,
    NODE_SEEN,
    NODE_UP,
};

static void flag_update_dm_intf(intf_t* intf) {
    amxd_object_t* obj = NULL;
    char* flag_str = NULL;
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    obj = intf_object(intf);
    if(obj == NULL) {
        SAH_TRACEZ_ERROR(ME, "Could not find datamodel object for intf %s", intf_name(intf));
        goto exit;
    }

    flag_str = amxc_set_to_string(intf_flagset(intf));
    amxd_trans_select_object(&trans, obj);
    amxd_trans_set_value(cstring_t, &trans, "Flags", flag_str);

    if(amxd_trans_apply(&trans, nm_get_dm()) != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Failed to update flags for intf %s", intf_name(intf));
    }

exit:
    free(flag_str);
    amxd_trans_clean(&trans);
    return;
}

static bool flag_step(intf_t* intf, void* data) {
    bool ret = false;
    flag_ctx_t* ctx = (flag_ctx_t*) data;
    amxc_set_t* flagset = intf_flagset(intf);

    if(amxp_expr_eval_set(ctx->condition, flagset, NULL)) {
        switch(ctx->op) {
        case SET_FLAG:
            amxc_set_union(flagset, ctx->flagset);
            break;
        case CLEAR_FLAG:
            amxc_set_subtract(flagset, ctx->flagset);
            break;
        case HAS_FLAG:
            amxc_set_union(ctx->flagset, flagset);
            break;
        }
        if(ctx->op != HAS_FLAG) {
            flag_update_dm_intf(intf);
        }
    }

    return ret;
}

static unsigned long node_is_up(traverse_node_t* node, amxp_expr_t* condition) {
    intf_t* intf = traverse_node_intf(node);
    traverse_edge_t* edge = NULL;

    if(traverse_node_marker(node) == NODE_NEW) {
        traverse_node_mark(node, NODE_SEEN);
        if((intf != NULL) && amxp_expr_eval_set(condition, intf_flagset(intf), NULL)) {
            if(amxc_set_has_flag(intf_flagset(intf), "up")) {
                traverse_node_mark(node, NODE_UP);
            } else {
                traverse_node_mark(node, NODE_DOWN);
                return NODE_DOWN;
            }
        }
        for(edge = traverse_node_first_edge(node); edge != NULL; edge = traverse_edge_next(edge)) {
            switch(node_is_up(traverse_edge_node(edge), condition)) {
            case NODE_UP:
                traverse_node_mark(node, NODE_UP);
                return NODE_UP;
            case NODE_DOWN:
                traverse_node_mark(node, NODE_DOWN);
                break;
            default:
                break;
            }
        }
    }
    return traverse_node_marker(node);
}

static amxd_status_t flag_execute_op(amxd_object_t* object,
                                     amxc_var_t* args,
                                     amxc_var_t* ret,
                                     flag_op_t op) {
    amxd_status_t status = amxd_status_unknown_error;
    traverse_mode_t mode;
    intf_t* intf = NULL;
    traverse_tree_t* tree = NULL;
    flag_ctx_t ctx;
    amxp_expr_t flagexp;
    amxp_expr_t condition;
    amxc_set_t flagset;

    amxc_set_init(&flagset, false);

    if(op != HAS_FLAG) {
        when_false(arg_get_set(&flagset, args, "flag", &status), exit);
    } else {
        when_false(arg_get_expr(&flagexp, args, "flag", &status), exit);
    }
    when_false(arg_get_expr(&condition, args, "condition", &status), exit_exp);
    when_false(arg_get_traverse(&mode, args, "traverse", &status), exit_cond);

    intf = intf_get_priv(object);
    if(intf == NULL) {
        SAH_TRACEZ_ERROR(ME, "failed to get interface");
        status = amxd_status_parameter_not_found;
        goto exit_cond;
    }

    tree = traverse_tree_create(mode, intf);
    if(tree == NULL) {
        SAH_TRACEZ_ERROR(ME, "failed to create traverse tree");
        goto exit_cond;
    }

    ctx.flagset = &flagset;
    ctx.condition = &condition;
    ctx.op = op;
    traverse_tree_walk(tree, flag_step, &ctx);
    traverse_tree_destroy(tree);

    if(op == HAS_FLAG) {
        amxc_var_set(bool, ret, amxp_expr_eval_set(&flagexp, ctx.flagset, NULL));
    }

    status = amxd_status_ok;

exit_cond:
    amxp_expr_clean(&condition);
exit_exp:
    if(op == HAS_FLAG) {
        amxp_expr_clean(&flagexp);
    }
exit:
    amxc_set_clean(&flagset);
    return status;
}

amxd_status_t _setFlag(amxd_object_t* object,
                       UNUSED amxd_function_t* func,
                       amxc_var_t* args,
                       amxc_var_t* ret) {
    return flag_execute_op(object, args, ret, SET_FLAG);
}

amxd_status_t _clearFlag(amxd_object_t* object,
                         UNUSED amxd_function_t* func,
                         amxc_var_t* args,
                         amxc_var_t* ret) {
    return flag_execute_op(object, args, ret, CLEAR_FLAG);
}

amxd_status_t _hasFlag(amxd_object_t* object,
                       UNUSED amxd_function_t* func,
                       amxc_var_t* args,
                       amxc_var_t* ret) {
    return flag_execute_op(object, args, ret, HAS_FLAG);
}

amxd_status_t _isUp(amxd_object_t* object,
                    UNUSED amxd_function_t* func,
                    amxc_var_t* args,
                    amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    amxp_expr_t condition;
    traverse_mode_t mode;
    intf_t* intf = NULL;
    traverse_tree_t* tree = NULL;

    when_false(arg_get_expr(&condition, args, "flag", &status), exit);
    when_false(arg_get_traverse(&mode, args, "traverse", &status), exit_expr);

    intf = intf_get_priv(object);
    if(intf == NULL) {
        SAH_TRACEZ_ERROR(ME, "failed to get interface");
        status = amxd_status_parameter_not_found;
        goto exit_expr;
    }

    tree = traverse_tree_create(mode, intf);
    if(tree == NULL) {
        SAH_TRACEZ_ERROR(ME, "failed to create traverse tree");
        goto exit_expr;
    }

    amxc_var_set(bool, ret, node_is_up(tree, &condition) == NODE_UP);

    traverse_tree_destroy(tree);

    status = amxd_status_ok;

exit_expr:
    amxp_expr_clean(&condition);
exit:
    return status;
}
