/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "intf.h"
#include "query.h"

#define ME "netmodel"

struct _intf {
    amxc_llist_it_t it;
    char* name;
    amxc_llist_t llintfs;
    amxc_llist_t ulintfs;
    amxc_set_t* flagset;
    amxd_object_t* object;
    amxc_llist_t queries;
};

struct _intf_link {
    amxc_llist_it_t it;
    intf_t* intf;
};

typedef struct _linklistener {
    amxc_llist_it_t it;
    intf_t* ulintf;
    intf_t* llintf;
    intf_linklistener function;
    void* userdata;
} linklistener_t;

static amxc_llist_t intfs = {NULL, NULL};

static amxc_llist_t linklisteners = {NULL, NULL};

static void intf_close_query(amxc_llist_it_t* it) {
    query_t* q = amxc_container_of(it, query_t, intf_it);

    query_destroy(q);
}

intf_t* intf_create(const char* name) {
    intf_t* intf = NULL;

    when_str_empty(name, error);

    if(intf_find(name) != NULL) {
        SAH_TRACEZ_ERROR(ME, "intf %s already exists", name);
        return NULL;
    }

    intf = (intf_t*) calloc(1, sizeof(intf_t));
    when_null(intf, error);

    intf->name = strdup(name);
    when_str_empty(intf->name, error);

    amxc_set_new(&intf->flagset, false);

    amxc_llist_append(&intfs, &intf->it);
    return intf;

error:
    if(intf != NULL) {
        free(intf->name);
    }
    free(intf);
    return NULL;
}

void intf_destroy(intf_t* intf) {
    amxd_object_t* obj = NULL;

    if(intf == NULL) {
        return;
    }

    while(!amxc_llist_is_empty(&intf->llintfs)) {
        intf_unlink(intf, intf_llintfs(intf)->intf);
    }

    while(!amxc_llist_is_empty(&intf->ulintfs)) {
        intf_unlink(intf_ulintfs(intf)->intf, intf);
    }

    amxc_llist_for_each(it, &linklisteners) {
        linklistener_t* ll = amxc_container_of(it, linklistener_t, it);
        if((ll->llintf == intf) || (ll->ulintf == intf)) {
            amxc_llist_it_take(it);
            free(ll);
        }
    }

    amxc_llist_clean(&intf->queries, intf_close_query);

    obj = intf_object(intf);
    if(obj != NULL) {
        obj->priv = NULL;
    }

    amxc_set_delete(&intf->flagset);
    amxc_llist_it_take(&intf->it);
    free(intf->name);
    free(intf);
}

intf_t* intf_find(const char* name) {
    intf_t* intf = NULL;

    when_str_empty(name, out);

    for(intf = intf_first(); intf; intf = intf_next(intf)) {
        if(!strcmp(intf->name, name)) {
            return intf;
        }
    }

out:
    return NULL;
}

intf_t* intf_first(void) {
    return amxc_container_of(amxc_llist_get_first(&intfs), intf_t, it);
}

intf_t* intf_next(intf_t* intf) {
    if(intf == NULL) {
        SAH_TRACEZ_ERROR(ME, "intf is NULL");
        return NULL;
    }
    return amxc_container_of(amxc_llist_it_get_next(&intf->it), intf_t, it);
}

const char* intf_name(intf_t* intf) {
    if(intf == NULL) {
        SAH_TRACEZ_ERROR(ME, "intf is NULL");
        return NULL;
    }
    return intf->name;
}

amxc_set_t* intf_flagset(intf_t* intf) {
    if(intf == NULL) {
        SAH_TRACEZ_ERROR(ME, "intf is NULL");
        return NULL;
    }
    return intf->flagset;
}

intf_link_t* intf_llintfs(intf_t* intf) {
    if(intf == NULL) {
        SAH_TRACEZ_ERROR(ME, "intf is NULL");
        return NULL;
    }
    return amxc_container_of(amxc_llist_get_first(&intf->llintfs), intf_link_t, it);
}

intf_link_t* intf_ulintfs(intf_t* intf) {
    if(intf == NULL) {
        SAH_TRACEZ_ERROR(ME, "intf is NULL");
        return NULL;
    }
    return amxc_container_of(amxc_llist_get_first(&intf->ulintfs), intf_link_t, it);
}

amxd_object_t* intf_object(intf_t* intf) {
    if(intf == NULL) {
        SAH_TRACEZ_ERROR(ME, "intf is NULL");
        return NULL;
    }
    return intf->object;
}

void intf_object_set(intf_t* intf, amxd_object_t* object) {
    if(intf == NULL) {
        SAH_TRACEZ_ERROR(ME, "intf is NULL");
        return;
    }
    intf->object = object;
}

intf_t* intf_link_current(intf_link_t* link) {
    if(link == NULL) {
        SAH_TRACEZ_ERROR(ME, "link is NULL");
        return NULL;
    }
    return link->intf;
}

intf_link_t* intf_link_next(intf_link_t* link) {
    if(link == NULL) {
        SAH_TRACEZ_ERROR(ME, "link is NULL");
        return NULL;
    }
    return amxc_container_of(amxc_llist_it_get_next(&link->it), intf_link_t, it);
}

static intf_link_t* intf_link_add(amxc_llist_t* list, intf_t* intf) {
    intf_link_t* link = (intf_link_t*) calloc(1, sizeof(intf_link_t));
    if(link == NULL) {
        SAH_TRACEZ_ERROR(ME, "calloc failed");
        return NULL;
    }
    link->intf = intf;
    amxc_llist_append(list, &link->it);
    return link;
}

static void intf_link_del(intf_link_t* link) {
    amxc_llist_it_take(&link->it);
    free(link);
}

static void intf_link_notify(intf_t* ulintf, intf_t* llintf, bool value) {
    linklistener_t* l = NULL;
    linklistener_t* lnext = NULL;

    amxc_llist_it_t*(* start)(const amxc_llist_t* list) = value ? amxc_llist_get_first : amxc_llist_get_last;
    amxc_llist_it_t*(* step)(const amxc_llist_it_t* it) = value ? amxc_llist_it_get_next : amxc_llist_it_get_previous;

    for(l = amxc_container_of(start(&linklisteners), linklistener_t, it); l; l = lnext) {
        lnext = amxc_container_of(step(&l->it), linklistener_t, it);
        if(l->ulintf && (l->ulintf != ulintf)) {
            continue;
        }
        if(l->llintf && (l->llintf != llintf)) {
            continue;
        }
        l->function(ulintf, llintf, value, l->userdata);
    }
}

void intf_link(intf_t* ulintf, intf_t* llintf) {
    intf_link_t* ulink = NULL;
    intf_link_t* llink = NULL;

    when_null(ulintf, error);
    when_null(llintf, error);

    for(ulink = intf_llintfs(ulintf); ulink; ulink = intf_link_next(ulink)) {
        if(ulink->intf == llintf) {
            break;
        }
    }
    if(ulink == NULL) {
        ulink = intf_link_add(&ulintf->llintfs, llintf);
        if(ulink == NULL) {
            SAH_TRACEZ_ERROR(ME, "Failed to add ulintf->llintf link");
            return;
        }
    }
    for(llink = intf_ulintfs(llintf); llink; llink = intf_link_next(llink)) {
        if(llink->intf == ulintf) {
            break;
        }
    }
    if(llink == NULL) {
        if(intf_link_add(&llintf->ulintfs, ulintf) == NULL) {
            SAH_TRACEZ_ERROR(ME, "Failed to add llintf->ulintf link");
            intf_link_del(ulink); // so consistency is guaranteed, even on out of memory exceptions
            return;
        }
        intf_link_notify(ulintf, llintf, true); // llintf->ulintf was not found -> notify
    }

error:
    return;
}

void intf_unlink(intf_t* ulintf, intf_t* llintf) {
    intf_link_t* link = NULL;

    when_null(ulintf, error);
    when_null(llintf, error);

    for(link = intf_llintfs(ulintf); link; link = intf_link_next(link)) {
        if(link->intf == llintf) {
            intf_link_del(link);
            break;
        }
    }

    for(link = intf_ulintfs(llintf); link; link = intf_link_next(link)) {
        if(link->intf == ulintf) { // llintf->ulintf was found -> notify
            intf_link_del(link);
            intf_link_notify(ulintf, llintf, false);
            break;
        }
    }

error:
    return;
}

static void intf_flood_linklistener(intf_t* ulintf, intf_t* llintf, intf_linklistener l, void* userdata, bool value) {
    intf_t* in = NULL;
    intf_link_t* link = NULL;

    for(in = intf_first(); in; in = intf_next(in)) {
        if(ulintf && (in != ulintf)) {
            continue;
        }
        for(link = intf_llintfs(in); link; link = intf_link_next(link)) {
            if(llintf && (link->intf != llintf)) {
                continue;
            }
            l(in, link->intf, value, userdata);
        }
    }
}

static bool intf_equals_linklistener(linklistener_t* l, intf_t* ulintf, intf_t* llintf, intf_linklistener function, void* userdata) {
    if(l->ulintf != ulintf) {
        return false;
    }
    if(l->llintf != llintf) {
        return false;
    }
    if(l->function != function) {
        return false;
    }
    if(l->userdata != userdata) {
        return false;
    }
    return true;
}

void intf_add_linklistener(intf_t* ulintf, intf_t* llintf, intf_linklistener l, void* userdata, bool boost) {
    linklistener_t* linklistener = NULL;

    when_null(l, error);
    linklistener = (linklistener_t*) calloc(1, sizeof(linklistener_t));
    when_null(linklistener, error);

    linklistener->ulintf = ulintf;
    linklistener->llintf = llintf;
    linklistener->function = l;
    linklistener->userdata = userdata;
    amxc_llist_append(&linklisteners, &linklistener->it);

    if(boost) {
        intf_flood_linklistener(ulintf, llintf, l, userdata, true);
    }

error:
    return;
}

void intf_del_linklistener(intf_t* ulintf, intf_t* llintf, intf_linklistener l, void* userdata, bool teardown) {

    when_null(l, error);

    amxc_llist_iterate(it, &linklisteners) {
        linklistener_t* ll = amxc_container_of(it, linklistener_t, it);
        if(intf_equals_linklistener(ll, ulintf, llintf, l, userdata)) {
            amxc_llist_it_take(it);
            free(ll);
            break;
        }
    }

    if(teardown) {
        intf_flood_linklistener(ulintf, llintf, l, userdata, false);
    }

error:
    return;
}

amxc_llist_t* intf_queries(intf_t* intf) {
    amxc_llist_t* list = NULL;

    when_null_trace(intf, exit, ERROR, "Intf is NULL");

    list = &intf->queries;

exit:
    return list;
}
