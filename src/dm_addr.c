/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <debug/sahtrace.h>

#include "intf.h"
#include "traverse.h"
#include "arg.h"
#include "dm_intf.h"

#define ME "netmodel"
#define str_not_empty(x) (((x == NULL) || (*x == '\0')) == false)

typedef struct _addr_family {
    const char* obj_name;
    const char* flag;
    const char* delegate;
} addr_family_t;

static addr_family_t addr_family_ipv4 = { "IPv4Address", "ipv4", NULL};
static addr_family_t addr_family_ipv6 = { "IPv6Address", "ipv6", "IPv6AddressDelegate"};

typedef struct _intf_addr_ctx {
    amxc_var_t var;
    amxp_expr_t condition;
    bool single_addr;
    bool address_only;
    addr_family_t* family;
} intf_addr_ctx_t;

static bool get_delegate_addr_step(const char* delegate_interface, intf_addr_ctx_t* ctx);

static char* get_netdev_name(amxd_object_t* obj) {
    amxc_var_t args;
    amxc_var_t retval;
    char* ret = NULL;

    amxc_var_init(&args);
    amxc_var_init(&retval);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "name", "NetDevName");
    amxd_object_invoke_function(obj, "getFirstParameter", &args, &retval);

    if(amxc_var_is_null(&retval)) {
        ret = strdup("");
    } else {
        ret = amxc_var_dyncast(cstring_t, &retval);
    }

    amxc_var_clean(&args);
    amxc_var_clean(&retval);

    return ret;
}

static amxd_status_t execute_intf_addr_function(amxd_object_t* object,
                                                amxc_var_t* args,
                                                bool (* step)(intf_t* intf, void* userdata),
                                                intf_addr_ctx_t* ctx) {
    amxd_status_t status = amxd_status_unknown_error;
    traverse_mode_t mode;
    intf_t* intf = intf_get_priv(object);
    traverse_tree_t* tree = NULL;

    if(intf == NULL) {
        SAH_TRACEZ_ERROR(ME, "failed to get interface");
        status = amxd_status_parameter_not_found;
        goto exit;
    }
    when_false(arg_get_traverse(&mode, args, "traverse", &status), exit);
    when_false(arg_get_expr(&ctx->condition, args, "flag", &status), exit);

    tree = traverse_tree_create(mode, intf);
    if(tree == NULL) {
        SAH_TRACEZ_ERROR(ME, "failed to create traverse tree");
        goto exit_expr;
    }

    ctx->family = &addr_family_ipv4;
    traverse_tree_walk(tree, step, ctx);
    traverse_tree_clear_marks(tree);

    ctx->family = &addr_family_ipv6;
    traverse_tree_walk(tree, step, ctx);
    traverse_tree_destroy(tree);

    status = amxd_status_ok;

exit_expr:
    amxp_expr_clean(&ctx->condition);
exit:
    return status;
}

static int get_addr_info(amxc_var_t* info,
                         amxd_object_t* nm_intf,
                         amxd_object_t* ip_addr,
                         const char* intf_name,
                         intf_addr_ctx_t* ctx) {
    amxc_set_t flagset;
    amxc_var_t* param = NULL;
    amxc_string_t str;
    int ret = -1;
    const char* status = GET_CHAR(amxd_object_get_param_value(ip_addr, "Status"), NULL);
    const char* scope = NULL;
    const char* flags = NULL;
    const char* type_flags = NULL;
    char* netdev_name = NULL;

    amxc_string_init(&str, 0);
    amxc_set_init(&flagset, false);

    when_false(GET_BOOL(amxd_object_get_param_value(ip_addr, "Enable"), NULL), exit);
    when_str_empty(status, exit);
    when_false((strcmp(status, "Enabled") == 0), exit);

    scope = GET_CHAR(amxd_object_get_param_value(ip_addr, "Scope"), NULL);
    when_null(scope, exit);
    flags = GET_CHAR(amxd_object_get_param_value(ip_addr, "Flags"), NULL);
    when_null(flags, exit);
    type_flags = GET_CHAR(amxd_object_get_param_value(ip_addr, "TypeFlags"), NULL);
    when_null(type_flags, exit);
    amxc_string_setf(&str, "%s %s %s %s", ctx->family->flag, scope, flags, type_flags);
    amxc_set_parse(&flagset, amxc_string_get(&str, 0));
    when_false(amxp_expr_eval_set(&ctx->condition, &flagset, NULL), exit);

    netdev_name = get_netdev_name(nm_intf);
    amxc_var_add_key(cstring_t, info, "NetDevName", netdev_name);
    amxc_var_add_key(cstring_t, info, "NetModelIntf", intf_name);
    amxc_var_add_key(cstring_t, info, "Family", ctx->family->flag);
    amxc_var_add_key(cstring_t, info, "Scope", scope);
    amxc_var_add_key(cstring_t, info, "Flags", flags);
    amxc_var_add_key(cstring_t, info, "TypeFlags", type_flags);
    param = amxc_var_add_new_key(info, "Peer");
    amxd_object_get_param(ip_addr, "Peer", param);
    param = amxc_var_add_new_key(info, "PrefixLen");
    amxd_object_get_param(ip_addr, "PrefixLen", param);
    param = amxc_var_add_new_key(info, "Address");
    amxd_object_get_param(ip_addr, "IPAddress", param);

    ret = 0;

exit:
    amxc_set_clean(&flagset);
    amxc_string_clean(&str);
    free(netdev_name);
    return ret;
}

static bool get_addr_step(intf_t* intf, void* data) {
    intf_addr_ctx_t* ctx = (intf_addr_ctx_t*) data;
    amxd_object_t* obj = intf_object(intf);
    amxd_object_t* addr_obj = NULL;

    when_null(obj, exit);

    if(str_not_empty(ctx->family->delegate)) {
        // Search for a delegate
        const char* delegate = GET_CHAR(amxd_object_get_param_value(obj, ctx->family->delegate), NULL);
        if(str_not_empty(delegate)) {
            bool result = get_delegate_addr_step(delegate, ctx);
            return result;
        }
    }

    addr_obj = amxd_object_get(obj, ctx->family->obj_name);
    when_null(addr_obj, exit);

    amxd_object_iterate(instance, it, addr_obj) {
        amxd_object_t* instance = amxc_container_of(it, amxd_object_t, it);
        amxc_var_t info;

        amxc_var_init(&info);
        amxc_var_set_type(&info, AMXC_VAR_ID_HTABLE);
        if(get_addr_info(&info, obj, instance, intf_name(intf), ctx) != 0) {
            amxc_var_clean(&info);
            continue;
        }

        if(ctx->single_addr) {
            if(ctx->address_only) {
                amxc_var_move(&ctx->var, GET_ARG(&info, "Address"));
            } else {
                amxc_var_move(&ctx->var, &info);
            }
            amxc_var_clean(&info);
            return true;
        } else {
            amxc_var_t* var = amxc_var_add_new(&ctx->var);
            amxc_var_move(var, &info);
            amxc_var_clean(&info);
        }
    }

exit:
    return false;
}

static bool get_delegate_addr_step(const char* delegate_interface, intf_addr_ctx_t* ctx) {
    amxc_llist_t nm_list;
    amxd_path_t path;
    bool result = false;

    amxc_llist_init(&nm_list);

    when_failed_status(amxd_path_init(&path, delegate_interface), exit, amxd_path_clean(&path); );

    /* Found delegate, get corresponding NetModel Intf */
    amxd_dm_resolve_pathf(nm_get_dm(), &nm_list, "NetModel.Intf.['Device.%s' in InterfacePath]", skip_prefix(&path));
    amxd_path_clean(&path);
    amxc_llist_iterate(list_item, &nm_list) {
        amxd_object_t* nm_obj = amxd_dm_findf(nm_get_dm(), "%s", amxc_string_get(amxc_string_from_llist_it(list_item), 0));

        intf_t* intf = intf_get_priv(nm_obj);
        if(get_addr_step(intf, ctx)) {
            result = true;
            break;
        }
    }
exit:
    amxc_llist_clean(&nm_list, amxc_string_list_it_free);
    return result;
}

amxd_status_t _getAddrs(amxd_object_t* object,
                        UNUSED amxd_function_t* func,
                        amxc_var_t* args,
                        amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    intf_addr_ctx_t ctx;
    ctx.single_addr = false;
    ctx.address_only = false;

    amxc_var_init(&ctx.var);
    amxc_var_set_type(&ctx.var, AMXC_VAR_ID_LIST);

    status = execute_intf_addr_function(object, args, get_addr_step, &ctx);
    when_failed(status, exit);

    amxc_var_move(ret, &ctx.var);

exit:
    amxc_var_clean(&ctx.var);
    return status;
}

amxd_status_t _luckyAddr(amxd_object_t* object,
                         UNUSED amxd_function_t* func,
                         amxc_var_t* args,
                         amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    intf_addr_ctx_t ctx;
    ctx.single_addr = true;
    ctx.address_only = false;

    amxc_var_init(&ctx.var);

    status = execute_intf_addr_function(object, args, get_addr_step, &ctx);
    when_failed(status, exit);

    amxc_var_move(ret, &ctx.var);

exit:
    amxc_var_clean(&ctx.var);
    return status;
}

amxd_status_t _luckyAddrAddress(amxd_object_t* object,
                                UNUSED amxd_function_t* func,
                                amxc_var_t* args,
                                amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    intf_addr_ctx_t ctx;
    ctx.single_addr = true;
    ctx.address_only = true;

    amxc_var_init(&ctx.var);

    status = execute_intf_addr_function(object, args, get_addr_step, &ctx);
    when_failed(status, exit);

    amxc_var_move(ret, &ctx.var);

exit:
    amxc_var_clean(&ctx.var);
    return status;
}
