/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <debug/sahtrace.h>

#include "intf.h"
#include "traverse.h"
#include "arg.h"
#include "dm_intf.h"

#define ME "netmodel"

typedef struct _intf_ctx {
    amxc_var_t var;
    amxp_expr_t condition;
    intf_t* target_intf;
} intf_ctx_t;

static amxd_status_t execute_intf_function(amxd_object_t* object,
                                           amxc_var_t* args,
                                           amxc_var_t* ret,
                                           bool (* step)(intf_t* intf, void* userdata),
                                           intf_ctx_t* ctx) {
    amxd_status_t status = amxd_status_unknown_error;
    traverse_mode_t mode;
    intf_t* intf = intf_get_priv(object);
    traverse_tree_t* tree = NULL;

    if(intf == NULL) {
        SAH_TRACEZ_ERROR(ME, "failed to get interface");
        status = amxd_status_parameter_not_found;
        goto exit;
    }
    when_false(arg_get_traverse(&mode, args, "traverse", &status), exit);

    tree = traverse_tree_create(mode, intf);
    if(tree == NULL) {
        SAH_TRACEZ_ERROR(ME, "failed to create traverse tree");
        goto exit;
    }

    traverse_tree_walk(tree, step, ctx);
    traverse_tree_destroy(tree);

    amxc_var_move(ret, &ctx->var);
    status = amxd_status_ok;

exit:
    amxc_var_clean(&ctx->var);
    return status;
}

static bool is_linked_step(intf_t* intf, void* data) {
    intf_ctx_t* ctx = (intf_ctx_t*) data;

    if(intf == ctx->target_intf) {
        amxc_var_set(bool, &ctx->var, true);
        return true;
    }

    return false;
}

static bool get_intfs_step(intf_t* intf, void* data) {
    intf_ctx_t* ctx = (intf_ctx_t*) data;

    if(amxp_expr_eval_set(&ctx->condition, intf_flagset(intf), NULL)) {
        amxc_var_add(cstring_t, &ctx->var, intf_name(intf));
    }

    return false;
}

static bool lucky_intf_step(intf_t* intf, void* data) {
    intf_ctx_t* ctx = (intf_ctx_t*) data;

    if(amxp_expr_eval_set(&ctx->condition, intf_flagset(intf), NULL)) {
        amxc_var_set(cstring_t, &ctx->var, intf_name(intf));
        return true;
    }

    return false;
}

static bool get_mibs_step(intf_t* intf, void* data) {
    intf_ctx_t* ctx = (intf_ctx_t*) data;
    amxd_object_t* obj = NULL;
    char* mibs = NULL;

    when_false(amxp_expr_eval_set(&ctx->condition, intf_flagset(intf), NULL), exit);

    obj = amxd_dm_findf(nm_get_dm(), "NetModel.Intf.%s.", intf_name(intf));
    when_null(obj, exit);

    mibs = amxd_object_get_mibs(obj);
    amxc_var_add_key(cstring_t, &ctx->var, intf_name(intf), mibs != NULL ? mibs : "");
    free(mibs);

exit:
    return false;
}

amxd_status_t _isLinkedTo(amxd_object_t* object,
                          UNUSED amxd_function_t* func,
                          amxc_var_t* args,
                          amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    intf_ctx_t ctx;

    amxc_var_init(&ctx.var);
    amxc_var_set_type(&ctx.var, AMXC_VAR_ID_BOOL);
    amxc_var_set(bool, &ctx.var, false);

    // If no valid target argument is found, return false
    when_false(arg_get_intf(&ctx.target_intf, args, "target", &status), exit);
    status = execute_intf_function(object, args, ret, is_linked_step, &ctx);

    return status;

exit:
    amxc_var_move(ret, &ctx.var);
    status = amxd_status_ok;
    amxc_var_clean(&ctx.var);
    return status;
}

amxd_status_t _getIntfs(amxd_object_t* object,
                        UNUSED amxd_function_t* func,
                        amxc_var_t* args,
                        amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    intf_ctx_t ctx;

    amxc_var_init(&ctx.var);
    amxc_var_set_type(&ctx.var, AMXC_VAR_ID_LIST);

    when_false(arg_get_expr(&ctx.condition, args, "flag", &status), exit);
    status = execute_intf_function(object, args, ret, get_intfs_step, &ctx);

exit:
    amxp_expr_clean(&ctx.condition);
    return status;
}

amxd_status_t _luckyIntf(amxd_object_t* object,
                         UNUSED amxd_function_t* func,
                         amxc_var_t* args,
                         amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    intf_ctx_t ctx;

    amxc_var_init(&ctx.var);
    amxc_var_set_type(&ctx.var, AMXC_VAR_ID_CSTRING);

    when_false(arg_get_expr(&ctx.condition, args, "flag", &status), exit);
    status = execute_intf_function(object, args, ret, lucky_intf_step, &ctx);

exit:
    amxp_expr_clean(&ctx.condition);
    return status;
}

amxd_status_t _getMibs(amxd_object_t* object,
                       UNUSED amxd_function_t* func,
                       amxc_var_t* args,
                       amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    intf_ctx_t ctx;

    amxc_var_init(&ctx.var);
    amxc_var_set_type(&ctx.var, AMXC_VAR_ID_HTABLE);

    when_false(arg_get_expr(&ctx.condition, args, "flag", &status), exit);
    status = execute_intf_function(object, args, ret, get_mibs_step, &ctx);

exit:
    amxp_expr_clean(&ctx.condition);
    return status;
}


/**
 * @brief This function is currently only intended to be called by the statmon client.
 * It allows statmon to send events as if they are coming from NetModel.
 */
amxd_status_t _sendEvent(amxd_object_t* object,
                         UNUSED amxd_function_t* func,
                         amxc_var_t* args,
                         UNUSED amxc_var_t* ret) {

    amxd_object_emit_signal(object, "nm:statmon", args);

    return amxd_status_ok;
}