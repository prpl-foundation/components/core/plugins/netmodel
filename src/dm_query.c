/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "dm_intf.h"
#include "dm_query.h"
#include "query.h"
#include "traverse.h"

#define ME "netmodel"

typedef enum {
    QUERY_OPEN,
    QUERY_ALTER,
    QUERY_TRIGGER,
    QUERY_CLOSE
} query_op_t;

typedef struct {
    enum query_update_reason reason;
    const amxc_set_t* set;
    traverse_mode_t traverse_mode;
} query_ctx_t;

static void query_emit_updated(amxd_object_t* obj, query_t* q) {
    amxc_var_t sig_data;
    amxc_var_t* data = NULL;

    amxc_var_init(&sig_data);
    amxc_var_set_type(&sig_data, AMXC_VAR_ID_HTABLE);
    data = amxc_var_add_new_key(&sig_data, "Result");
    amxc_var_copy(data, query_result(q));

    amxd_object_emit_signal(obj, "query", &sig_data);

    amxc_var_clean(&sig_data);
}

static amxd_status_t query_update_dm(query_t* q, query_op_t op) {
    amxd_trans_t trans;
    amxd_status_t status = amxd_status_unknown_error;
    amxd_object_t* obj = NULL;
    amxd_object_t* instance = NULL;
    char* str = NULL;

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    obj = amxd_object_get(query_intf(q), "Query.");
    when_null(obj, exit);
    instance = amxd_object_get_instance(obj, NULL, query_id(q));
    amxd_trans_select_object(&trans, obj);

    switch(op) {
    case QUERY_OPEN:
        amxd_trans_add_inst(&trans, query_id(q), NULL);
        str = query_describe(q);
        amxd_trans_set_value(cstring_t, &trans, "Description", str);
        break;
    case QUERY_ALTER:
        when_null(instance, exit);
        amxd_trans_select_object(&trans, instance);
        str = amxc_set_to_string(query_subscribers(q));
        amxd_trans_set_value(cstring_t, &trans, "Subscribers", str);
        break;
    case QUERY_TRIGGER:
        when_null(instance, exit);
        amxd_trans_select_object(&trans, instance);
        str = amxc_var_dyncast(cstring_t, query_result(q));
        if(str == NULL) {
            str = strdup("");
        }
        amxd_trans_set_value(cstring_t, &trans, "ResultString", str);
        query_emit_updated(instance, q);
        break;
    case QUERY_CLOSE:
        when_null(instance, exit);
        amxd_trans_del_inst(&trans, query_id(q), NULL);
        break;
    }

    status = amxd_trans_apply(&trans, nm_get_dm());
    if(status != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Failed to update query object, status = %d", status);
    }
    free(str);

exit:
    amxd_trans_clean(&trans);

    return status;
}

static void query_open_handler(query_t* q, UNUSED void* userdata) {
    if(query_update_dm(q, QUERY_OPEN) != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Failed to create Query instance");
    }
}

static void query_close_handler(UNUSED query_t* q, UNUSED void* userdata) {
    if(query_update_dm(q, QUERY_CLOSE) != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Failed to delete Query instance");
    }
}

static void query_alter_handler(query_t* q, UNUSED void* userdata) {
    if(query_update_dm(q, QUERY_ALTER) != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Failed to update Query subscribers");
    }
}

static void query_trigger_handler(query_t* q, UNUSED void* userdata) {
    if(query_update_dm(q, QUERY_TRIGGER) != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Failed to update Query result");
    }
}

static query_listener_t query_listener = {
    query_open_handler,
    query_close_handler,
    query_alter_handler,
    query_trigger_handler
};

static bool query_argument_contains_flags(const amxc_set_t* set, const char* arg) {
    bool ret = false;
    amxc_string_t str;
    amxp_expr_t exp;
    amxp_expr_status_t status = amxp_expr_status_unknown_error;

    when_str_empty(arg, exit);
    when_null(set, exit);

    amxc_llist_iterate(it, &set->list) {
        amxc_flag_t* flag = amxc_container_of(it, amxc_flag_t, it);

        amxc_string_init(&str, 0);
        amxc_string_setf(&str, "'%s' matches '\\b%s\\b'", arg, flag->flag);

        status = amxp_expr_init(&exp, amxc_string_get(&str, 0));
        if(status != amxp_expr_status_ok) {
            SAH_TRACEZ_ERROR(ME, "Failed to initialize expression, status = %d", status);
            break;
        }

        if(amxp_expr_eval(&exp, &status)) {
            ret = true;
            break;
        }

        amxc_string_clean(&str);
        amxp_expr_clean(&exp);
    }

    amxc_string_clean(&str);
    amxp_expr_clean(&exp);

exit:
    return ret;
}

static uint32_t amxc_ts_diff_in_ms(const amxc_ts_t* t1, const amxc_ts_t* t2) {
    uint32_t diff = 0;

    when_false_trace(amxc_ts_compare(t1, t2) == -1, exit, WARNING, "T2 is equal/older than T1");

    diff = (t2->sec - t1->sec) * 1000;
    diff += (t2->nsec - t1->nsec) / 1000000;
    if(t2->nsec - t1->nsec < 0) {
        diff += 1000;
    }

exit:
    return diff;
}

static void update_query_single(query_t* q,
                                const amxc_set_t* set,
                                enum query_update_reason reason) {
    traverse_mode_t traverse = traverse_mode_this;
    netmodel_stats_t* stats = nm_get_stats();
    const char* str = GET_CHAR(query_args(q), "traverse");
    if(str == NULL) {
        str = "this";
    }
    traverse_mode_parse(&traverse, str);

    switch(reason) {
    case REASON_LINK:
        if(( traverse != traverse_mode_this) && ( traverse != traverse_mode_all)) {
            query_invalidate(q);
            stats->queries_retriggered[REASON_LINK]++;
        }
        break;
    case REASON_FLAG:
        // If any of the changed flags is present in the flag or condition args, retrigger
        // If the "up" flag has changed and the query class is isUp, also retrigger
        if((query_argument_contains_flags(set, GET_CHAR(query_args(q), "flag"))) ||
           (query_argument_contains_flags(set, GET_CHAR(query_args(q), "condition"))) ||
           (amxc_set_has_flag(set, "up") &&
            ( strcmp(query_class_function(query_class(q))->name, "isUp") == 0))) {
            query_invalidate(q);
            stats->queries_retriggered[REASON_FLAG]++;
        }
        break;
    case REASON_INTF:
        if(traverse == traverse_mode_all) {
            query_invalidate(q);
            stats->queries_retriggered[REASON_INTF]++;
        }
        break;
    case REASON_PARAM:
        // If the query class is getParameters or getFirstParameter
        // and any of the changed parameters is present in the name arg, retrigger
        if((strstr(query_class_function(query_class(q))->name, "Parameter") != NULL) &&
           amxc_set_has_flag(set, GET_CHAR(query_args(q), "name"))) {
            query_invalidate(q);
            stats->queries_retriggered[REASON_PARAM]++;
        }
        break;
    case REASON_MIB:
        if(strstr(query_class_function(query_class(q))->name, "Parameter") != NULL) {
            query_invalidate(q);
            stats->queries_retriggered[REASON_MIB]++;
        }
        break;
    case REASON_ADDR:
        if(strstr(query_class_function(query_class(q))->name, "Addr") != NULL) {
            query_invalidate(q);
            stats->queries_retriggered[REASON_ADDR]++;
        }
        break;
    case REASON_PATH:
        if(strcmp(query_class_function(query_class(q))->name, "resolvePath") == 0) {
            query_invalidate(q);
            stats->queries_retriggered[REASON_PATH]++;
        }
        break;
    case REASON_DHCP:
        if((strcmp(query_class_function(query_class(q))->name, "getDHCPOption") == 0) &&
           (amxc_set_has_flag(set, GET_CHAR(query_args(q), "type")))) {
            char* tag_str = amxc_var_dyncast(cstring_t, GET_ARG(query_args(q), "tag"));
            if(amxc_set_has_flag(set, tag_str)) {
                query_invalidate(q);
                stats->queries_retriggered[REASON_DHCP]++;
            }
            free(tag_str);
        }
        break;
    case REASON_PREFIX:
        if(strstr(query_class_function(query_class(q))->name, "IPv6Prefix") != NULL) {
            query_invalidate(q);
            stats->queries_retriggered[REASON_PREFIX]++;
        }
        break;
    default:
        SAH_TRACEZ_ERROR(ME, "Invalid query update reason");
        break;
    }
}

static void update_queries_traverse_all(const amxc_set_t* set, enum query_update_reason reason) {
    for(query_class_t* cl = query_class_first(); cl != NULL; cl = query_class_next(cl)) {
        for(query_t* q = query_first(cl); q != NULL; q = query_next(q)) {
            const char* str = GET_CHAR(query_args(q), "traverse");
            if((str != NULL) && (strcmp(str, "all") == 0)) {
                update_query_single(q, set, reason);
            }
        }
    }
}

static void update_queries_rp(const amxc_set_t* set) {
    netmodel_stats_t* stats = nm_get_stats();
    query_class_t* cl = query_class_find("resolvePath");
    query_t* q = NULL;
    char* path = amxc_set_to_string(set);
    amxc_string_t path_str;

    amxc_string_init(&path_str, 0);
    amxc_string_set(&path_str, path);
    for(q = query_first(cl); q != NULL; q = query_next(q)) {
        // Check if changed paths (to and from) are in the argument list of the query
        if(amxc_string_search(&path_str, GET_CHAR(query_args(q), "path"), 0) != -1) {
            query_invalidate(q);
            stats->queries_retriggered[REASON_PATH]++;
        }
    }

    free(path);
    amxc_string_clean(&path_str);
}

static void update_queries_addr(void) {
    for(query_class_t* cl = query_class_first(); cl != NULL; cl = query_class_next(cl)) {
        if(strstr(query_class_function(cl)->name, "Addr") == NULL) {
            continue;
        }

        for(query_t* q = query_first(cl); q != NULL; q = query_next(q)) {
            update_query_single(q, NULL, REASON_ADDR);
        }
    }
}

// Check if the traverse mode of the query includes the given traverse mode.
// The given traverse mode is starting from the interface which was changed.
// E.g. if that is "up exclusive", the query scope matches if it's down
// because from the query perspective the changed interface is lower in the hierarchy.
// Assumption: traverse will be only one of: this, down_exclusive, up_exclusive
static bool check_valid_traverse(query_t* q, traverse_mode_t traverse) {
    bool ret = false;
    traverse_mode_t q_traverse = traverse_mode_this;
    const char* str = GET_CHAR(query_args(q), "traverse");

    when_null(str, exit);
    when_false_trace(traverse_mode_parse(&q_traverse, str), exit, ERROR,
                     "Invalid traverse mode given");

    switch(traverse) {
    case traverse_mode_up_exclusive:
        if((q_traverse == traverse_mode_down) ||
           (q_traverse == traverse_mode_down_exclusive) ||
           (q_traverse == traverse_mode_one_level_down)) {
            ret = true;
        }
        break;
    case traverse_mode_down_exclusive:
        if((q_traverse == traverse_mode_up) ||
           (q_traverse == traverse_mode_up_exclusive) ||
           (q_traverse == traverse_mode_one_level_up)) {
            ret = true;
        }
        break;
    case traverse_mode_this:
        if((q_traverse == traverse_mode_up) ||
           (q_traverse == traverse_mode_down) ||
           (q_traverse == traverse_mode_this)) {
            ret = true;
        }
        break;
    default:
        break;
    }

exit:
    return ret;
}

static bool query_step(intf_t* intf, void* data) {
    bool ret = false;
    query_ctx_t* ctx = (query_ctx_t*) data;

    amxc_llist_iterate(it, intf_queries(intf)) {
        query_t* q = amxc_container_of(it, query_t, intf_it);
        if(check_valid_traverse(q, ctx->traverse_mode)) {
            update_query_single(q, ctx->set, ctx->reason);
        }
    }

    return ret;
}

static void update_queries_hierarchy(intf_t* intf,
                                     const amxc_set_t* set,
                                     enum query_update_reason reason,
                                     traverse_mode_t traverse) {
    traverse_tree_t* tree = NULL;
    query_ctx_t ctx;

    ctx.reason = reason;
    ctx.set = set;
    ctx.traverse_mode = traverse;

    tree = traverse_tree_create(traverse, intf);
    traverse_tree_walk(tree, query_step, &ctx);
    traverse_tree_destroy(tree);
}

void init_queries(void) {
    query_add_listener(&query_listener, NULL);
    query_class_register("hasFlag");
    query_class_register("isUp");
    query_class_register("isLinkedTo");
    query_class_register("getIntfs");
    query_class_register("luckyIntf");
    query_class_register("getParameters");
    query_class_register("getFirstParameter");
    query_class_register("getAddrs");
    query_class_register("luckyAddr");
    query_class_register("luckyAddrAddress");
    query_class_register("resolvePath");
    query_class_register("getDHCPOption");
    query_class_register("getIPv6Prefixes");
    query_class_register("getFirstIPv6Prefix");
}

void cleanup_queries(void) {
    query_class_t* cl = query_class_first();

    query_del_listener(&query_listener, NULL);
    while(cl != NULL) {
        query_class_t* c = cl;
        cl = query_class_next(cl);
        query_class_unregister(c);
    }
}

void update_queries(intf_t* intf,
                    const amxc_set_t* set,
                    enum query_update_reason reason) {
    netmodel_stats_t* stats = nm_get_stats();
    amxc_ts_t time_start;
    amxc_ts_t time_stop;

    SAH_TRACEZ_INFO(ME, "Retriggering queries because of %s", query_update_reason_to_str(reason));
    amxc_ts_now(&time_start);

    // Check for resolve path queries first, they don't use hierarchies
    if(reason == REASON_PATH) {
        update_queries_rp(set);
        goto exit;
    }

    // Update the queries with traverse all, they must always be checked
    update_queries_traverse_all(set, reason);

    // In case of REASON_INTF, traverse_all is enough
    if(reason == REASON_INTF) {
        goto exit;
    }

    // Check address queries in case of IPv6,
    // they can affect multiple hierarchies if addressDelegates are used
    if((reason == REASON_ADDR) && (set != NULL)) {
        update_queries_addr();
        goto exit;
    }

    // In case of a link/unlink event
    // intf will be the upper link
    // in case of unlink, set will contain the name of the lower link
    if((reason == REASON_LINK) && (set != NULL)) {
        // If unlink -> extra hierarchy to check
        char* str = amxc_set_to_string(set);
        intf_t* llintf = intf_find(str);
        free(str);

        when_null_trace(llintf, exit, ERROR, "Could not find lower layer interface");

        update_queries_hierarchy(llintf, set, reason, traverse_mode_up_exclusive);
        update_queries_hierarchy(llintf, set, reason, traverse_mode_down_exclusive);
        update_queries_hierarchy(llintf, set, reason, traverse_mode_this);
    }

    // Retrigger queries found in the hierarchy of the changed intf
    update_queries_hierarchy(intf, set, reason, traverse_mode_up_exclusive);
    update_queries_hierarchy(intf, set, reason, traverse_mode_down_exclusive);
    update_queries_hierarchy(intf, set, reason, traverse_mode_this);

exit:
    amxc_ts_now(&time_stop);
    stats->query_time_spent[reason] += amxc_ts_diff_in_ms(&time_start, &time_stop);
    SAH_TRACEZ_INFO(ME, "Retriggering queries done");
    return;
}

amxd_status_t _openQuery(amxd_object_t* object,
                         UNUSED amxd_function_t* func,
                         amxc_var_t* args,
                         amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    amxc_var_t* sub_var = NULL;
    amxc_var_t* cl_var = NULL;
    const char* sub = NULL;
    query_t* query = NULL;
    query_class_t* cl = NULL;

    // Subscriber & class should not be passed to the query function args
    // so we take them out of the args variant
    sub_var = amxc_var_take_key(args, "subscriber");
    sub = amxc_var_constcast(cstring_t, sub_var);
    cl_var = amxc_var_take_key(args, "class");
    cl = query_class_find(amxc_var_constcast(cstring_t, cl_var));
    if(cl == NULL) {
        SAH_TRACEZ_ERROR(ME, "Invalid query class");
        status = amxd_status_function_not_implemented;
        goto exit;
    }

    if(!amxd_function_are_args_valid(query_class_function(cl), args)) {
        SAH_TRACEZ_ERROR(ME, "Invalid query function arguments");
        status = amxd_status_invalid_function_argument;
        goto exit;
    }

    query = query_open(sub, cl, object, args);
    when_null(query, exit);

    amxc_var_set(uint32_t, ret, query_id(query));
    status = amxd_status_ok;

exit:
    amxc_var_delete(&sub_var);
    amxc_var_delete(&cl_var);
    return status;
}

amxd_status_t _closeQuery(amxd_object_t* object,
                          UNUSED amxd_function_t* func,
                          amxc_var_t* args,
                          UNUSED amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    const char* sub = NULL;
    query_class_t* cl = NULL;
    amxc_var_t* sub_var = NULL;
    amxc_var_t* cl_var = NULL;

    // Subscriber & class should not be passed to the query function args
    // so we take them out of the args variant
    sub_var = amxc_var_take_key(args, "subscriber");
    cl_var = amxc_var_take_key(args, "class");
    sub = amxc_var_constcast(cstring_t, sub_var);
    cl = query_class_find(amxc_var_constcast(cstring_t, cl_var));
    if(cl == NULL) {
        SAH_TRACEZ_ERROR(ME, "Invalid query class");
        status = amxd_status_function_not_implemented;
        goto exit;
    }

    if(!amxd_function_are_args_valid(query_class_function(cl), args)) {
        SAH_TRACEZ_ERROR(ME, "Invalid query function arguments");
        status = amxd_status_invalid_function_argument;
        goto exit;
    }

    if(query_close(sub, cl, object, args) == 0) {
        SAH_TRACEZ_ERROR(ME, "Query does not exist");
        status = amxd_status_invalid_function_argument;
        goto exit;
    }

    status = amxd_status_ok;

exit:
    amxc_var_delete(&sub_var);
    amxc_var_delete(&cl_var);
    return status;
}

amxd_status_t _getResult(amxd_object_t* object,
                         UNUSED amxd_function_t* func,
                         UNUSED amxc_var_t* args,
                         amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    query_t* q = query_find(amxd_object_get_index(object));

    when_null(q, exit);

    amxc_var_copy(ret, query_result(q));
    status = amxd_status_ok;

exit:
    return status;
}

amxd_status_t _query_removed(amxd_object_t* object,
                             UNUSED amxd_param_t* param,
                             UNUSED amxd_action_t reason,
                             UNUSED const amxc_var_t* const args,
                             UNUSED amxc_var_t* const retval,
                             UNUSED void* priv) {
    query_t* q = query_find(amxd_object_get_index(object));
    if(q != NULL) {
        query_destroy(q);
    }

    return amxd_status_ok;
}
