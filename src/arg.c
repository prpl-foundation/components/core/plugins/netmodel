/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <debug/sahtrace.h>

#include "intf.h"
#include "arg.h"
#include "traverse.h"

#define ME "netmodel"

bool arg_get_set(amxc_set_t* set,
                 const amxc_var_t* args,
                 const char* name,
                 amxd_status_t* status) {
    if(amxc_set_parse(set, GET_CHAR(args, name)) != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to parse set arg \"%s\"", name);
        if(status != NULL) {
            *status = amxd_status_invalid_arg;
        }
        return false;
    }

    return true;
}

bool arg_get_expr(amxp_expr_t* expr,
                  const amxc_var_t* args,
                  const char* name,
                  amxd_status_t* status) {
    if(amxp_expr_init(expr, GET_CHAR(args, name)) != amxp_expr_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Failed to parse expr arg \"%s\"", name);
        if(status != NULL) {
            *status = amxd_status_invalid_expr;
        }
        return false;
    }

    return true;
}

bool arg_get_traverse(traverse_mode_t* mode,
                      const amxc_var_t* args,
                      const char* name,
                      amxd_status_t* status) {
    if(!traverse_mode_parse(mode, GET_CHAR(args, name))) {
        SAH_TRACEZ_ERROR(ME, "Failed to parse traverse arg \"%s\"", name);
        if(status != NULL) {
            *status = amxd_status_invalid_arg;
        }
        return false;
    }

    return true;
}

bool arg_get_intf(intf_t** intf,
                  const amxc_var_t* args,
                  const char* name,
                  amxd_status_t* status) {
    intf_t* target = intf_find(GET_CHAR(args, name));
    if(target == NULL) {
        SAH_TRACEZ_ERROR(ME, "Failed to parse intf arg \"%s\"", name);
        if(status != NULL) {
            *status = amxd_status_invalid_arg;
        }
        return false;
    }
    *intf = target;

    return true;
}

bool arg_get_string(const char** target,
                    const amxc_var_t* args,
                    const char* name,
                    amxd_status_t* status) {
    const char* str = GET_CHAR(args, name);
    if(str == NULL) {
        SAH_TRACEZ_ERROR(ME, "Failed to parse char arg \"%s\"", name);
        if(status != NULL) {
            *status = amxd_status_invalid_arg;
        }
        return false;
    }
    *target = str;

    return true;
}

bool arg_get_uint16(uint16_t* target,
                    const amxc_var_t* args,
                    const char* name,
                    amxd_status_t* status) {
    uint32_t value = GETP_UINT32(args, name);
    if(value == 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to parse uint16 arg \"%s\"", name);
        if(status != NULL) {
            *status = amxd_status_invalid_arg;
        }
        return false;
    }
    *target = value;

    return true;
}

bool arg_get_var(amxc_var_t* target,
                 const amxc_var_t* args,
                 const char* name,
                 amxd_status_t* status) {
    amxc_var_t* var = GET_ARG(args, name);
    if(var == NULL) {
        SAH_TRACEZ_ERROR(ME, "Failed to parse variant arg \"%s\"", name);
        if(status != NULL) {
            *status = amxd_status_invalid_arg;
        }
        return false;
    }
    amxc_var_move(target, var);

    return true;
}
