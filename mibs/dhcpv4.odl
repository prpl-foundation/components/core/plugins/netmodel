/*expr: dhcpv4 */
%define
{
    /**
     * MIB is loaded on all network devices that have (or had) a dhcp client enabled
     *
     * All devices matching expression: "dhcpv4" are extended with this MIB
     *
     * @version 1.0
     */
    mib dhcpv4 {
        /**
         * maps "ResetOnPhysDownTimeout"
         */
        int32 ResetOnPhysDownTimeout;

        /**
         * maps TR181 "DHCPStatus"
         * @version 1.0
         */
        string DHCPStatus;

        /**
         * maps TR181 "Status"
         * @version 1.0
         */
        string DHCPv4Status;

        /**
         * maps TR181 "IPAddress"
         * @version 1.0
         */
        string IPAddress;

        /**
         * maps TR181 "IPRouters"
         * @version 1.0
         */
        string IPRouters;

        /**
         * maps TR181 "DNSServers"
         * @version 1.0
         */
        string DNSServers;

        /**
         * maps TR181 "ReqOption"
         * @version 1.0
         */
        object ReqOption[] {
            /**
             * @version 1.0
             */
            %unique %key string Alias;

            /**
             * maps TR181 "Enable"
             * @version 1.0
             */
            bool Enable;

            /**
             * maps TR181 "Tag"
             * @version 1.0
             */
            uint8 Tag;

            /**
             * maps TR181 "Value"
             * @version 1.0
             */
            string Value;
        }
    }
}
