/*expr: statmon */
%define
{
    /**
     * All devices matching expression: "statmon" are extended with this MIB
     *
     * @version 1.0
     */
    mib statmon {
        object StatMon [] {
            /**
            * Enables the monitoring of the stats.
            * When enabled, an event containing the stats for the matching interface will send with a defined interval
            * @version 1.0
            */
            bool Enable;

            /**
            * Time in seconds between two events containing the latest stats
            * @version 1.0
            */
            uint32 Interval;

            /**
            * StatMon type decides where the statistics are coming from
            * @version 1.0
            */
            string Type = "unknown" {
                on action validate call check_enum ["unknown", "netdev"];
            }

            /**
            * This function allows the statmon client to send the events as if they are coming from NetModel itself
            * @version 1.0
            */
            void sendEvent(%in htable data);

            /**
            * Event that will be used to send out the stats updates
            * @version 1.0
            */
            event "nm:statmon";
        }
    }
}
