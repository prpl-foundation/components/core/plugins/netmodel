# netmodel

## Summary

Network Model of the system.
It provides an overview of all network interfaces, physical or logical.

## Description

This plugin manages the NetModel data model, controls the different NetModel interfaces,
loads and unloads MIBs depending on interface flags and keeps track of the
NetModel queries created upon its interfaces and notifies subscribers of updates.
