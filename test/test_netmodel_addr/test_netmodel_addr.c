/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>

#include <amxo/amxo.h>

#include "intf.h"
#include "query.h"
#include "dm_intf.h"
#include "dm_query.h"

#include "test_netmodel_addr.h"
#include "test_utility.h"

static amxd_dm_t dm;
static amxo_parser_t parser;
static const char* odl_defs = "../../odl/netmodel_definition.odl";

static amxd_status_t invoke_dm_func_f_t(amxd_object_t* obj,
                                        const char* func,
                                        const char* flag,
                                        const char* traverse,
                                        amxc_var_t* ret) {
    amxc_var_t args;
    amxd_status_t status = amxd_status_unknown_error;

    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "flag", flag);
    amxc_var_add_key(cstring_t, &args, "traverse", traverse);
    status = invoke_dm_func(obj, func, &args, ret);
    amxc_var_clean(&args);

    return status;
}

static amxd_status_t invoke_dm_func_n_v_f_t(amxd_object_t* obj,
                                            const char* func,
                                            const char* name,
                                            amxc_var_t* value,
                                            const char* flag,
                                            const char* traverse,
                                            amxc_var_t* ret) {
    amxc_var_t args;
    amxc_var_t* val = NULL;
    amxd_status_t status = amxd_status_unknown_error;

    amxc_var_init(&args);
    amxc_var_copy(val, value);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "name", name);
    amxc_var_add_key(cstring_t, &args, "flag", flag);
    amxc_var_add_key(cstring_t, &args, "traverse", traverse);
    val = amxc_var_add_new_key(&args, "value");
    amxc_var_copy(val, value);
    status = invoke_dm_func(obj, func, &args, ret);
    amxc_var_clean(&args);

    return status;
}

static bool is_ipv4_addr(const amxc_var_t* const data) {
    if(amxc_var_type_of(data) != AMXC_VAR_ID_HTABLE) {
        return false;
    }

    if((GET_UINT32(data, "PrefixLen") != 4) ||
       (strcmp(GET_CHAR(data, "Scope"), "Scopev4") != 0) ||
       (strcmp(GET_CHAR(data, "Flags"), "Flagsv4") != 0) ||
       (strcmp(GET_CHAR(data, "Peer"), "Peerv4") != 0) ||
       (strcmp(GET_CHAR(data, "NetModelIntf"), "intf_0") != 0) ||
       (strcmp(GET_CHAR(data, "Address"), "192.168.1.1") != 0) ||
       (strcmp(GET_CHAR(data, "NetDevName"), "NetDev_Instance") != 0) ||
       (strcmp(GET_CHAR(data, "Family"), "ipv4") != 0)) {
        return false;
    } else {
        return true;
    }
}

static bool is_ipv6_addr(const amxc_var_t* const data) {
    if(amxc_var_type_of(data) != AMXC_VAR_ID_HTABLE) {
        return false;
    }

    if((GET_UINT32(data, "PrefixLen") != 6) ||
       (strcmp(GET_CHAR(data, "Scope"), "Scopev6") != 0) ||
       (strcmp(GET_CHAR(data, "Flags"), "Flagsv6") != 0) ||
       (strcmp(GET_CHAR(data, "Peer"), "Peerv6") != 0) ||
       (strcmp(GET_CHAR(data, "NetModelIntf"), "intf_0") != 0) ||
       (strcmp(GET_CHAR(data, "Address"), "abcd::1") != 0) ||
       (strcmp(GET_CHAR(data, "NetDevName"), "NetDev_Instance") != 0) ||
       (strcmp(GET_CHAR(data, "Family"), "ipv6") != 0)) {
        return false;
    } else {
        return true;
    }
}

static bool is_ipv6_delegate_addr(const amxc_var_t* const data) {
    if(amxc_var_type_of(data) != AMXC_VAR_ID_HTABLE) {
        return false;
    }

    if((GET_UINT32(data, "PrefixLen") != 6) ||
       (strcmp(GET_CHAR(data, "Scope"), "Scopev6") != 0) ||
       (strcmp(GET_CHAR(data, "Flags"), "Flagsv6") != 0) ||
       (strcmp(GET_CHAR(data, "Peer"), "Peerv6") != 0) ||
       (strcmp(GET_CHAR(data, "NetModelIntf"), "intf_0") != 0) ||
       (strcmp(GET_CHAR(data, "Address"), "1:abcd::1") != 0) ||
       (strcmp(GET_CHAR(data, "NetDevName"), "NetDev_Instance") != 0) ||
       (strcmp(GET_CHAR(data, "Family"), "ipv6") != 0)) {
        return false;
    } else {
        return true;
    }
}

static bool is_ipv6_prefix(const amxc_var_t* const data) {
    if(amxc_var_type_of(data) != AMXC_VAR_ID_HTABLE) {
        return false;
    }

    if((strcmp(GET_CHAR(data, "Prefix"), "this is not a valid prefix") != 0) ||
       (strcmp(GET_CHAR(data, "Origin"), "test mib odl") != 0) ||
       (strcmp(GET_CHAR(data, "StaticType"), "PrefixDelegation") != 0) ||
       (strcmp(GET_CHAR(data, "PrefixStatus"), "Invalid") != 0) ||
       (strcmp(GET_CHAR(data, "PreferredLifetime"), "9999-12-31T23:59:59Z") != 0) ||
       (strcmp(GET_CHAR(data, "ValidLifetime"), "9999-12-31T23:59:59Z") != 0) ||
       (GET_UINT32(data, "RelativePreferredLifetime") != 2500) ||
       (GET_UINT32(data, "RelativeValidLifetime") != 3600) ||
       (GET_BOOL(data, "Autonomous") != 1) ||
       (GET_BOOL(data, "OnLink") != 1)) {
        return false;
    } else {
        return true;
    }
}

static bool check_ipv4_present(const amxc_var_t* const data) {
    bool ret = false;

    amxc_var_for_each(var, data) {
        if(is_ipv4_addr(var)) {
            ret = true;
            break;
        }
    }

    return ret;
}

static bool check_ipv6_present(const amxc_var_t* const data) {
    bool ret = false;

    amxc_var_for_each(var, data) {
        if(is_ipv6_addr(var)) {
            ret = true;
            break;
        }
    }

    return ret;
}

static bool check_ipv6_delegated_present(const amxc_var_t* const data) {
    bool ret = false;

    amxc_var_for_each(var, data) {
        if(is_ipv6_delegate_addr(var)) {
            ret = true;
            break;
        }
    }

    return ret;
}

static bool check_ipv6_prefix_present(const amxc_var_t* const data) {
    bool ret = false;

    amxc_var_for_each(var, data) {
        if(is_ipv6_prefix(var)) {
            ret = true;
            break;
        }
    }

    return ret;
}
int test_addr_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;
    amxd_trans_t trans;

    assert_int_equal(amxd_trans_init(&trans), 0);

    test_init_dm(&dm, &parser);
    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    assert_int_equal(amxo_parser_parse_file(&parser, odl_defs, root_obj), 0);
    assert_int_equal(amxo_parser_scan_mib_dir(&parser, "./"), 0);

    handle_events();

    assert_int_equal(_nm_main(0, &dm, &parser), 0);

    amxd_trans_select_pathf(&trans, "NetModel.Intf.");
    amxd_trans_add_inst(&trans, 0, "intf_0");
    amxd_trans_set_value(cstring_t, &trans, "Flags", "test_ip");

    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    handle_events();

    amxd_trans_clean(&trans);
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "NetModel.Intf.1.IPv4Address.");
    amxd_trans_add_inst(&trans, 1, "");
    amxd_trans_select_pathf(&trans, "NetModel.Intf.1.IPv6Address.");
    amxd_trans_add_inst(&trans, 1, "");
    amxd_trans_select_pathf(&trans, "NetModel.Intf.1.IPv6Prefix.");
    amxd_trans_add_inst(&trans, 1, "");
    amxd_trans_select_pathf(&trans, "NetModel.Intf.1.IPv6Prefix.");
    amxd_trans_add_inst(&trans, 2, "");
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    handle_events();

    amxd_trans_clean(&trans);
    return 0;
}

int test_addr_teardown(UNUSED void** state) {
    assert_int_equal(_nm_main(1, &dm, &parser), 0);
    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    return 0;
}

void test_addr_getAddrs(UNUSED void** state) {
    amxd_object_t* intf_obj = amxd_dm_findf(&dm, "NetModel.Intf.intf_0");
    amxc_var_t ret;
    amxc_var_t var;

    assert_non_null(intf_obj);
    amxc_var_init(&ret);
    amxc_var_init(&var);

    assert_int_equal(invoke_dm_func_f_t(intf_obj, "getAddrs", "", "down", &ret), 0);
    assert_true(check_ipv4_present(&ret));
    assert_true(check_ipv6_present(&ret));
    assert_true(amxc_var_type_of(&ret) == AMXC_VAR_ID_LIST);
    assert_int_equal(amxc_llist_size(amxc_var_constcast(amxc_llist_t, &ret)), 2);

    assert_int_equal(invoke_dm_func_f_t(intf_obj, "getAddrs", "ipv4", "down", &ret), 0);
    assert_true(check_ipv4_present(&ret));
    assert_false(check_ipv6_present(&ret));
    assert_true(amxc_var_type_of(&ret) == AMXC_VAR_ID_LIST);
    assert_int_equal(amxc_llist_size(amxc_var_constcast(amxc_llist_t, &ret)), 1);

    assert_int_equal(invoke_dm_func_f_t(intf_obj, "getAddrs", "ipv6", "down", &ret), 0);
    assert_false(check_ipv4_present(&ret));
    assert_true(check_ipv6_present(&ret));
    assert_true(amxc_var_type_of(&ret) == AMXC_VAR_ID_LIST);
    assert_int_equal(amxc_llist_size(amxc_var_constcast(amxc_llist_t, &ret)), 1);

    assert_int_equal(invoke_dm_func_f_t(intf_obj, "getAddrs", "invalid_flag", "down", &ret), 0);
    assert_false(check_ipv4_present(&ret));
    assert_false(check_ipv6_present(&ret));
    assert_true(amxc_var_type_of(&ret) == AMXC_VAR_ID_LIST);
    assert_int_equal(amxc_llist_size(amxc_var_constcast(amxc_llist_t, &ret)), 0);

    amxc_var_set(bool, &var, false);
    assert_int_equal(invoke_dm_func_n_v_f_t(intf_obj, "setParameters", "IPv4Address.1.Enable", &var, NULL, NULL, &ret), 0);
    assert_int_equal(invoke_dm_func_f_t(intf_obj, "getAddrs", "ipv4", "down", &ret), 0);
    assert_false(check_ipv4_present(&ret));
    assert_false(check_ipv6_present(&ret));
    assert_true(amxc_var_type_of(&ret) == AMXC_VAR_ID_LIST);
    assert_int_equal(amxc_llist_size(amxc_var_constcast(amxc_llist_t, &ret)), 0);

    amxc_var_set(bool, &var, true);
    assert_int_equal(invoke_dm_func_n_v_f_t(intf_obj, "setParameters", "IPv4Address.1.Enable", &var, NULL, NULL, &ret), 0);
    assert_int_equal(invoke_dm_func_f_t(intf_obj, "getAddrs", "ipv4", "down", &ret), 0);
    assert_true(check_ipv4_present(&ret));
    assert_false(check_ipv6_present(&ret));
    assert_true(amxc_var_type_of(&ret) == AMXC_VAR_ID_LIST);
    assert_int_equal(amxc_llist_size(amxc_var_constcast(amxc_llist_t, &ret)), 1);

    amxc_var_clean(&ret);
    amxc_var_clean(&var);
}

void test_addr_luckyAddr(UNUSED void** state) {
    amxd_object_t* intf_obj = amxd_dm_findf(&dm, "NetModel.Intf.intf_0");
    amxc_var_t ret;
    amxc_var_t var;

    assert_non_null(intf_obj);
    amxc_var_init(&ret);
    amxc_var_init(&var);

    assert_int_equal(invoke_dm_func_f_t(intf_obj, "luckyAddr", "", "down", &ret), 0);
    assert_true(amxc_var_type_of(&ret) == AMXC_VAR_ID_HTABLE);

    assert_int_equal(invoke_dm_func_f_t(intf_obj, "luckyAddr", "ipv4", "down", &ret), 0);
    assert_true(amxc_var_type_of(&ret) == AMXC_VAR_ID_HTABLE);
    assert_true(is_ipv4_addr(&ret));

    assert_int_equal(invoke_dm_func_f_t(intf_obj, "luckyAddr", "ipv6", "down", &ret), 0);
    assert_true(amxc_var_type_of(&ret) == AMXC_VAR_ID_HTABLE);
    assert_true(is_ipv6_addr(&ret));

    assert_int_equal(invoke_dm_func_f_t(intf_obj, "luckyAddr", "invalid_flag", "down", &ret), 0);
    assert_false(is_ipv4_addr(&ret));
    assert_false(is_ipv6_addr(&ret));

    amxc_var_clean(&ret);
    amxc_var_clean(&var);
}

void test_addr_luckyAddrAddress(UNUSED void** state) {
    amxd_object_t* intf_obj = amxd_dm_findf(&dm, "NetModel.Intf.intf_0");
    amxc_var_t ret;
    amxc_var_t var;

    assert_non_null(intf_obj);
    amxc_var_init(&ret);
    amxc_var_init(&var);

    assert_int_equal(invoke_dm_func_f_t(intf_obj, "luckyAddrAddress", "", "down", &ret), 0);
    assert_true(amxc_var_type_of(&ret) == AMXC_VAR_ID_CSTRING);

    assert_int_equal(invoke_dm_func_f_t(intf_obj, "luckyAddrAddress", "ipv4", "down", &ret), 0);
    assert_true(amxc_var_type_of(&ret) == AMXC_VAR_ID_CSTRING);
    assert_int_equal(strcmp(amxc_var_constcast(cstring_t, &ret), "192.168.1.1"), 0);

    assert_int_equal(invoke_dm_func_f_t(intf_obj, "luckyAddrAddress", "ipv6", "down", &ret), 0);
    assert_true(amxc_var_type_of(&ret) == AMXC_VAR_ID_CSTRING);
    assert_int_equal(strcmp(amxc_var_constcast(cstring_t, &ret), "abcd::1"), 0);

    assert_int_equal(invoke_dm_func_f_t(intf_obj, "luckyAddrAddress", "invalid_flag", "down", &ret), 0);

    amxc_var_clean(&ret);
    amxc_var_clean(&var);
}

void test_prefix_getIPv6Prefixes(UNUSED void** state) {
    amxd_object_t* intf_obj = amxd_dm_findf(&dm, "NetModel.Intf.intf_0");
    amxc_var_t ret;
    amxc_var_t var;

    assert_non_null(intf_obj);
    amxc_var_init(&ret);
    amxc_var_init(&var);

    assert_int_equal(invoke_dm_func_f_t(intf_obj, "getIPv6Prefixes", "", "down", &ret), 0);
    assert_true(check_ipv6_prefix_present(&ret));
    assert_true(amxc_var_type_of(&ret) == AMXC_VAR_ID_LIST);
    assert_int_equal(amxc_llist_size(amxc_var_constcast(amxc_llist_t, &ret)), 2);

    assert_int_equal(invoke_dm_func_f_t(intf_obj, "getIPv6Prefixes", "invalid_flag", "down", &ret), 0);
    assert_false(check_ipv6_prefix_present(&ret));
    assert_true(amxc_var_type_of(&ret) == AMXC_VAR_ID_LIST);
    assert_int_equal(amxc_llist_size(amxc_var_constcast(amxc_llist_t, &ret)), 0);

    amxc_var_set(bool, &var, false);
    assert_int_equal(invoke_dm_func_n_v_f_t(intf_obj, "setParameters", "IPv6Prefix.1.Enable", &var, NULL, NULL, &ret), 0);
    assert_int_equal(invoke_dm_func_f_t(intf_obj, "getIPv6Prefixes", "", "down", &ret), 0);
    assert_true(check_ipv6_prefix_present(&ret));
    assert_true(amxc_var_type_of(&ret) == AMXC_VAR_ID_LIST);
    assert_int_equal(amxc_llist_size(amxc_var_constcast(amxc_llist_t, &ret)), 1);

    amxc_var_set(cstring_t, &var, "Disabled");
    assert_int_equal(invoke_dm_func_n_v_f_t(intf_obj, "setParameters", "IPv6Prefix.2.Status", &var, NULL, NULL, &ret), 0);
    assert_int_equal(invoke_dm_func_f_t(intf_obj, "getIPv6Prefixes", "", "down", &ret), 0);
    assert_false(check_ipv6_prefix_present(&ret));
    assert_true(amxc_var_type_of(&ret) == AMXC_VAR_ID_LIST);
    assert_int_equal(amxc_llist_size(amxc_var_constcast(amxc_llist_t, &ret)), 0);

    amxc_var_clean(&ret);
    amxc_var_clean(&var);
}

void test_prefix_getFirstIPv6Prefix(UNUSED void** state) {
    amxd_object_t* intf_obj = amxd_dm_findf(&dm, "NetModel.Intf.intf_0");
    amxc_var_t ret;
    amxc_var_t var;

    assert_non_null(intf_obj);
    amxc_var_init(&ret);
    amxc_var_init(&var);

    assert_int_equal(invoke_dm_func_f_t(intf_obj, "getFirstIPv6Prefix", "", "down", &ret), 0);
    assert_true(amxc_var_type_of(&ret) == AMXC_VAR_ID_HTABLE);
    assert_true(is_ipv6_prefix(&ret));

    assert_int_equal(invoke_dm_func_f_t(intf_obj, "luckyAddr", "invalid_flag", "down", &ret), 0);
    assert_false(is_ipv6_prefix(&ret));

    amxc_var_clean(&ret);
    amxc_var_clean(&var);
}

void test_addr_getAddrs_with_delegate(UNUSED void** state) {
    amxd_trans_t trans;

    assert_int_equal(amxd_trans_init(&trans), 0);

    amxd_trans_select_pathf(&trans, "NetModel.Intf.");
    amxd_trans_add_inst(&trans, 0, "intf_1");
    amxd_trans_set_value(cstring_t, &trans, "Flags", "test_ip");

    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    handle_events();

    amxd_trans_clean(&trans);
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "NetModel.Intf.2.");
    amxd_trans_set_value(cstring_t, &trans, "IPv6AddressDelegate", "Device.IP.Interface.3.");
    amxd_trans_select_pathf(&trans, "NetModel.Intf.2.IPv4Address.");
    amxd_trans_add_inst(&trans, 1, "");
    amxd_trans_select_pathf(&trans, "NetModel.Intf.2.IPv6Address.");
    amxd_trans_add_inst(&trans, 1, "");
    amxd_trans_select_pathf(&trans, "NetModel.Intf.2.IPv6Prefix.");
    amxd_trans_add_inst(&trans, 1, "");
    amxd_trans_select_pathf(&trans, "NetModel.Intf.2.IPv6Prefix.");
    amxd_trans_add_inst(&trans, 2, "");
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    handle_events();

    amxd_trans_clean(&trans);
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "NetModel.Intf.1.");
    amxd_trans_set_value(cstring_t, &trans, "InterfacePath", "Device.IP.Interface.3.");
    amxd_trans_select_pathf(&trans, "NetModel.Intf.1.IPv6Address.1.");
    amxd_trans_set_value(cstring_t, &trans, "IPAddress", "1:abcd::1");
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    handle_events();

    amxd_trans_clean(&trans);

    amxd_object_t* intf_obj = amxd_dm_findf(&dm, "NetModel.Intf.intf_1");
    amxc_var_t ret;
    amxc_var_t var;

    assert_non_null(intf_obj);
    amxc_var_init(&ret);
    amxc_var_init(&var);

    assert_int_equal(invoke_dm_func_f_t(intf_obj, "getAddrs", "ipv6", "down", &ret), 0);
    assert_true(check_ipv6_delegated_present(&ret));
    assert_true(amxc_var_type_of(&ret) == AMXC_VAR_ID_LIST);
    assert_int_equal(amxc_llist_size(amxc_var_constcast(amxc_llist_t, &ret)), 1);

    amxc_var_clean(&ret);
    amxc_var_clean(&var);
}
