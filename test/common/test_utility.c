/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>

#include <amxo/amxo.h>

#include "dm_intf.h"
#include "dm_query.h"
#include "test_utility.h"

static void test_init_function_resolvers(amxo_parser_t* parser) {
    amxo_resolver_ftab_add(parser, "interface_instance_added", AMXO_FUNC(_interface_instance_added));
    amxo_resolver_ftab_add(parser, "interface_added", AMXO_FUNC(_interface_added));
    amxo_resolver_ftab_add(parser, "interface_removed", AMXO_FUNC(_interface_removed));
    amxo_resolver_ftab_add(parser, "linkIntfs", AMXO_FUNC(_linkIntfs));
    amxo_resolver_ftab_add(parser, "unlinkIntfs", AMXO_FUNC(_unlinkIntfs));
    amxo_resolver_ftab_add(parser, "scanMibDir", AMXO_FUNC(_scanMibDir));
    amxo_resolver_ftab_add(parser, "setFlag", AMXO_FUNC(_setFlag));
    amxo_resolver_ftab_add(parser, "clearFlag", AMXO_FUNC(_clearFlag));
    amxo_resolver_ftab_add(parser, "hasFlag", AMXO_FUNC(_hasFlag));
    amxo_resolver_ftab_add(parser, "getResult", AMXO_FUNC(_getResult));
    amxo_resolver_ftab_add(parser, "openQuery", AMXO_FUNC(_openQuery));
    amxo_resolver_ftab_add(parser, "closeQuery", AMXO_FUNC(_closeQuery));
    amxo_resolver_ftab_add(parser, "query_removed", AMXO_FUNC(_query_removed));
    amxo_resolver_ftab_add(parser, "isUp", AMXO_FUNC(_isUp));
    amxo_resolver_ftab_add(parser, "isLinkedTo", AMXO_FUNC(_isLinkedTo));
    amxo_resolver_ftab_add(parser, "getIntfs", AMXO_FUNC(_getIntfs));
    amxo_resolver_ftab_add(parser, "luckyIntf", AMXO_FUNC(_luckyIntf));
    amxo_resolver_ftab_add(parser, "getParameters", AMXO_FUNC(_getParameters));
    amxo_resolver_ftab_add(parser, "setParameters", AMXO_FUNC(_setParameters));
    amxo_resolver_ftab_add(parser, "getFirstParameter", AMXO_FUNC(_getFirstParameter));
    amxo_resolver_ftab_add(parser, "setFirstParameter", AMXO_FUNC(_setFirstParameter));
    amxo_resolver_ftab_add(parser, "getAddrs", AMXO_FUNC(_getAddrs));
    amxo_resolver_ftab_add(parser, "luckyAddr", AMXO_FUNC(_luckyAddr));
    amxo_resolver_ftab_add(parser, "luckyAddrAddress", AMXO_FUNC(_luckyAddrAddress));
    amxo_resolver_ftab_add(parser, "intf_changed", AMXO_FUNC(_intf_changed));
    amxo_resolver_ftab_add(parser, "mib_changed", AMXO_FUNC(_mib_changed));
    amxo_resolver_ftab_add(parser, "addr_changed", AMXO_FUNC(_addr_changed));
    amxo_resolver_ftab_add(parser, "addr6_changed", AMXO_FUNC(_addr6_changed));
    amxo_resolver_ftab_add(parser, "resolvePath", AMXO_FUNC(_resolvePath));
    amxo_resolver_ftab_add(parser, "path_changed", AMXO_FUNC(_path_changed));
    amxo_resolver_ftab_add(parser, "getDHCPOption", AMXO_FUNC(_getDHCPOption));
    amxo_resolver_ftab_add(parser, "dhcp_changed", AMXO_FUNC(_dhcp_changed));
    amxo_resolver_ftab_add(parser, "getMibs", AMXO_FUNC(_getMibs));
    amxo_resolver_ftab_add(parser, "prefix_changed", AMXO_FUNC(_prefix_changed));
    amxo_resolver_ftab_add(parser, "getIPv6Prefixes", AMXO_FUNC(_getIPv6Prefixes));
    amxo_resolver_ftab_add(parser, "getFirstIPv6Prefix", AMXO_FUNC(_getFirstIPv6Prefix));
    amxo_resolver_ftab_add(parser, "getLinkInformation", AMXO_FUNC(_getLinkInformation));
    amxo_resolver_ftab_add(parser, "getStats", AMXO_FUNC(_getStats));
}

void test_init_dm(amxd_dm_t* dm, amxo_parser_t* parser) {
    assert_int_equal(amxd_dm_init(dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(parser), 0);

    test_init_function_resolvers(parser);
}

void handle_events(void) {
    while(amxp_signal_read() == 0) {
    }
}

amxd_status_t invoke_dm_func(amxd_object_t* intf_obj,
                             const char* func,
                             amxc_var_t* args,
                             amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;

    amxc_var_clean(ret);

    status = amxd_object_invoke_function(intf_obj, func, args, ret);
    handle_events();

    return status;
}
