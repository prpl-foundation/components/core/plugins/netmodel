/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>

#include <amxo/amxo.h>

#include "intf.h"
#include "dm_intf.h"

#include "test_netmodel_flag.h"
#include "test_utility.h"

static amxd_dm_t dm;
static amxo_parser_t parser;
static const char* odl_defs = "../../odl/netmodel_definition.odl";

static const int nr_of_intfs = 3;

int test_flag_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;
    int retval = 0;
    amxd_trans_t trans;
    int i = 0;
    char name[64];

    assert_int_equal(amxd_trans_init(&trans), 0);

    test_init_dm(&dm, &parser);
    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    retval = amxo_parser_parse_file(&parser, odl_defs, root_obj);
    printf("PARSER MESSAGE = %s\n", amxc_string_get(&parser.msg, 0));
    assert_int_equal(retval, 0);

    handle_events();

    assert_int_equal(_nm_main(0, &dm, &parser), 0);

    amxd_trans_select_pathf(&trans, "NetModel.Intf.");

    for(i = 0; i < nr_of_intfs; i++) {
        snprintf(name, sizeof(name), "intf_%d", i);
        amxd_trans_add_inst(&trans, 0, name);
        amxd_trans_select_pathf(&trans, ".^");
    }

    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    handle_events();

    intf_link(intf_find("intf_0"), intf_find("intf_1"));
    intf_link(intf_find("intf_1"), intf_find("intf_2"));
    handle_events();

    amxd_trans_clean(&trans);
    return 0;
}

int test_flag_teardown(UNUSED void** state) {
    assert_int_equal(_nm_main(1, &dm, &parser), 0);
    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    return 0;
}

static amxd_status_t invoke_flag_func(amxd_object_t* intf_obj,
                                      const char* func,
                                      const char* flag,
                                      const char* condition,
                                      const char* traverse,
                                      amxc_var_t* ret) {
    amxc_var_t args;
    amxc_var_init(&args);
    amxd_status_t status = amxd_status_unknown_error;

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "flag", flag);
    amxc_var_add_key(cstring_t, &args, "condition", condition);
    amxc_var_add_key(cstring_t, &args, "traverse", traverse);
    status = invoke_dm_func(intf_obj, func, &args, ret);

    amxc_var_clean(&args);

    return status;
}

void test_single_flag(UNUSED void** state) {
    amxd_object_t* intf_obj = amxd_dm_findf(&dm, "NetModel.Intf.intf_0");
    amxc_var_t ret;
    int i = 0;

    assert_non_null(intf_obj);
    amxc_var_init(&ret);

    // Add flag
    assert_int_equal(invoke_flag_func(intf_obj, "setFlag", "red", "", "this", &ret), 0);
    assert_int_equal(invoke_flag_func(intf_obj, "hasFlag", "red", "", "this", &ret), 0);
    assert_true(amxc_var_constcast(bool, &ret));
    // Clear the flag
    assert_int_equal(invoke_flag_func(intf_obj, "clearFlag", "red", "", "this", &ret), 0);
    assert_int_equal(invoke_flag_func(intf_obj, "hasFlag", "red", "", "this", &ret), 0);
    assert_false(amxc_var_constcast(bool, &ret));

    // Add flag
    assert_int_equal(invoke_flag_func(intf_obj, "setFlag", "red", "", "down", &ret), 0);
    for(i = 0; i < nr_of_intfs; i++) {
        amxd_object_t* obj = amxd_dm_findf(&dm, "NetModel.Intf.intf_%d", i);
        assert_non_null(obj);
        assert_int_equal(invoke_flag_func(obj, "hasFlag", "red", "", "this", &ret), 0);
        assert_true(amxc_var_constcast(bool, &ret));
    }
    // Clear the flag
    assert_int_equal(invoke_flag_func(intf_obj, "clearFlag", "red", "", "down", &ret), 0);
    for(i = 0; i < nr_of_intfs; i++) {
        amxd_object_t* obj = amxd_dm_findf(&dm, "NetModel.Intf.intf_%d", i);
        assert_non_null(obj);
        assert_int_equal(invoke_flag_func(obj, "hasFlag", "red", "", "this", &ret), 0);
        assert_false(amxc_var_constcast(bool, &ret));
    }
    // Verify that the flag is cleared, using traverse
    assert_int_equal(invoke_flag_func(intf_obj, "hasFlag", "red", "", "down", &ret), 0);
    assert_false(amxc_var_constcast(bool, &ret));

    // Add flag for single interface
    assert_int_equal(invoke_flag_func(intf_obj, "setFlag", "red", "", "this", &ret), 0);
    assert_int_equal(invoke_flag_func(intf_obj, "hasFlag", "red", "", "this", &ret), 0);
    assert_true(amxc_var_constcast(bool, &ret));
    // Add "blue" flag for intfs not containing the "red" flag
    assert_int_equal(invoke_flag_func(intf_obj, "setFlag", "blue", "!red", "down", &ret), 0);
    assert_int_equal(invoke_flag_func(intf_obj, "hasFlag", "blue", "", "this", &ret), 0);
    assert_false(amxc_var_constcast(bool, &ret));
    for(i = 1; i < nr_of_intfs; i++) {
        amxd_object_t* obj = amxd_dm_findf(&dm, "NetModel.Intf.intf_%d", i);
        assert_non_null(obj);
        assert_int_equal(invoke_flag_func(obj, "hasFlag", "blue", "", "this", &ret), 0);
        assert_true(amxc_var_constcast(bool, &ret));
    }
}

void test_multiple_flags(UNUSED void** state) {
    amxd_object_t* intf_obj = amxd_dm_findf(&dm, "NetModel.Intf.intf_0");
    amxc_var_t ret;

    assert_non_null(intf_obj);
    amxc_var_init(&ret);

    // Add flags
    assert_int_equal(invoke_flag_func(intf_obj, "setFlag", "red blue green", "", "this", &ret), 0);
    assert_int_equal(invoke_flag_func(intf_obj, "hasFlag", "red", "", "this", &ret), 0);
    assert_true(amxc_var_constcast(bool, &ret));
    assert_int_equal(invoke_flag_func(intf_obj, "hasFlag", "green", "", "this", &ret), 0);
    assert_true(amxc_var_constcast(bool, &ret));
    assert_int_equal(invoke_flag_func(intf_obj, "hasFlag", "blue", "", "this", &ret), 0);
    assert_true(amxc_var_constcast(bool, &ret));
    assert_int_equal(invoke_flag_func(intf_obj, "hasFlag", "green blue", "", "this", &ret), 0);
    assert_true(amxc_var_constcast(bool, &ret));
    assert_int_equal(invoke_flag_func(intf_obj, "hasFlag", "red green blue", "", "this", &ret), 0);
    assert_true(amxc_var_constcast(bool, &ret));
    // Clear the "red" flag
    assert_int_equal(invoke_flag_func(intf_obj, "clearFlag", "red", "", "this", &ret), 0);
    assert_int_equal(invoke_flag_func(intf_obj, "hasFlag", "red", "", "this", &ret), 0);
    assert_false(amxc_var_constcast(bool, &ret));
    assert_int_equal(invoke_flag_func(intf_obj, "hasFlag", "green blue", "", "this", &ret), 0);
    assert_true(amxc_var_constcast(bool, &ret));
    // Clear the other flags
    assert_int_equal(invoke_flag_func(intf_obj, "clearFlag", "green blue", "", "this", &ret), 0);
    assert_int_equal(invoke_flag_func(intf_obj, "hasFlag", "blue || green", "", "this", &ret), 0);
    assert_false(amxc_var_constcast(bool, &ret));
}

void test_up_flag(UNUSED void** state) {
    amxd_object_t* intf_obj = amxd_dm_findf(&dm, "NetModel.Intf.intf_0");
    amxc_var_t ret;
    amxd_trans_t trans;

    assert_non_null(intf_obj);
    amxc_var_init(&ret);

    assert_int_equal(invoke_flag_func(intf_obj, "hasFlag", "up", "", "this", &ret), 0);
    assert_false(GET_BOOL(&ret, NULL));

    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, intf_obj);
    amxd_trans_set_value(bool, &trans, "Status", true);
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    assert_int_equal(invoke_flag_func(intf_obj, "hasFlag", "up", "", "this", &ret), 0);
    assert_true(GET_BOOL(&ret, NULL));

    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, intf_obj);
    amxd_trans_set_value(bool, &trans, "Status", false);
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    assert_int_equal(invoke_flag_func(intf_obj, "hasFlag", "up", "", "this", &ret), 0);
    assert_false(GET_BOOL(&ret, NULL));
}

void test_enabled_flag(UNUSED void** state) {
    amxd_object_t* intf_obj = amxd_dm_findf(&dm, "NetModel.Intf.intf_0");
    amxc_var_t ret;
    amxd_trans_t trans;

    assert_non_null(intf_obj);
    amxc_var_init(&ret);

    assert_int_equal(invoke_flag_func(intf_obj, "hasFlag", "enabled", "", "this", &ret), 0);
    assert_false(GET_BOOL(&ret, NULL));

    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, intf_obj);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    assert_int_equal(invoke_flag_func(intf_obj, "hasFlag", "enabled", "", "this", &ret), 0);
    assert_true(GET_BOOL(&ret, NULL));

    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, intf_obj);
    amxd_trans_set_value(bool, &trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    assert_int_equal(invoke_flag_func(intf_obj, "hasFlag", "enabled", "", "this", &ret), 0);
    assert_false(GET_BOOL(&ret, NULL));
}
