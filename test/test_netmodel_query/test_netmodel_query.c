/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>

#include <amxo/amxo.h>

#include "intf.h"
#include "query.h"
#include "dm_intf.h"
#include "dm_query.h"

#include "test_netmodel_query.h"
#include "test_utility.h"

static amxd_dm_t dm;
static amxo_parser_t parser;
static const char* odl_defs = "../../odl/netmodel_definition.odl";

static const int nr_of_intfs = 1;

int test_query_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;
    int retval = 0;
    amxd_trans_t trans;
    int i = 0;
    char name[64];

    assert_int_equal(amxd_trans_init(&trans), 0);

    test_init_dm(&dm, &parser);
    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    retval = amxo_parser_parse_file(&parser, odl_defs, root_obj);
    printf("PARSER MESSAGE = %s\n", amxc_string_get(&parser.msg, 0));
    assert_int_equal(retval, 0);
    assert_int_equal(amxo_parser_scan_mib_dir(&parser, "./"), 0);

    handle_events();

    assert_int_equal(_nm_main(0, &dm, &parser), 0);

    amxd_trans_select_pathf(&trans, "NetModel.Intf.");

    for(i = 0; i < nr_of_intfs; i++) {
        snprintf(name, sizeof(name), "intf_%d", i);
        amxd_trans_add_inst(&trans, 0, name);
        amxd_trans_select_pathf(&trans, ".^");
    }

    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    handle_events();

    amxd_trans_clean(&trans);
    return 0;
}

int test_query_teardown(UNUSED void** state) {
    assert_int_equal(_nm_main(1, &dm, &parser), 0);
    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    return 0;
}

static amxd_status_t invoke_query_func(amxd_object_t* intf_obj,
                                       const char* func,
                                       const char* flag,
                                       const char* condition,
                                       const char* traverse,
                                       const char* subscriber,
                                       const char* cl,
                                       amxc_var_t* ret) {
    amxc_var_t args;
    amxc_var_init(&args);
    amxd_status_t status = amxd_status_unknown_error;

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "flag", flag);
    amxc_var_add_key(cstring_t, &args, "condition", condition);
    amxc_var_add_key(cstring_t, &args, "traverse", traverse);
    amxc_var_add_key(cstring_t, &args, "subscriber", subscriber);
    amxc_var_add_key(cstring_t, &args, "class", cl);
    status = invoke_dm_func(intf_obj, func, &args, ret);

    amxc_var_clean(&args);

    return status;
}

static amxd_status_t open_dhcp_query(amxd_object_t* intf_obj,
                                     const char* type,
                                     uint16_t tag,
                                     const char* traverse,
                                     const char* subscriber,
                                     amxc_var_t* ret) {
    amxc_var_t args;
    amxc_var_init(&args);
    amxd_status_t status = amxd_status_unknown_error;

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "type", type);
    amxc_var_add_key(uint16_t, &args, "tag", tag);
    amxc_var_add_key(cstring_t, &args, "traverse", traverse);
    amxc_var_add_key(cstring_t, &args, "subscriber", subscriber);
    amxc_var_add_key(cstring_t, &args, "class", "getDHCPOption");
    status = invoke_dm_func(intf_obj, "openQuery", &args, ret);

    amxc_var_clean(&args);

    return status;
}

static amxd_status_t open_getFirstParameter_query(amxd_object_t* intf_obj,
                                                  const char* name,
                                                  const char* traverse,
                                                  const char* subscriber,
                                                  amxc_var_t* ret) {
    amxc_var_t args;
    amxc_var_init(&args);
    amxd_status_t status = amxd_status_unknown_error;

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "name", name);
    amxc_var_add_key(cstring_t, &args, "traverse", traverse);
    amxc_var_add_key(cstring_t, &args, "subscriber", subscriber);
    amxc_var_add_key(cstring_t, &args, "class", "getFirstParameter");
    status = invoke_dm_func(intf_obj, "openQuery", &args, ret);

    amxc_var_clean(&args);

    return status;
}

static void check_query_open(amxc_var_t* ret, amxd_object_t* intf) {
    assert_true(amxc_var_type_of(ret) == AMXC_VAR_ID_UINT32);
    assert_int_not_equal(amxc_var_constcast(uint32_t, ret), 0);
    assert_non_null(amxd_object_findf(intf, "Query.%d.", amxc_var_constcast(uint32_t, ret)));
}

static void test_query_trigger_regexp(const char* expr, const char* flag, bool state) {
    amxd_object_t* intf_obj = amxd_dm_findf(&dm, "NetModel.Intf.intf_0");
    amxd_object_t* query_obj = NULL;
    amxc_var_t ret;
    uint32_t id = 0;

    assert_non_null(intf_obj);
    amxc_var_init(&ret);

    // Open query
    assert_int_equal(invoke_query_func(intf_obj, "openQuery", expr, NULL, "this", "me", "isUp", &ret), 0);
    check_query_open(&ret, intf_obj);
    id = amxc_var_constcast(uint32_t, &ret);
    query_obj = amxd_object_findf(intf_obj, "Query.%d.", id);

    // Get the query result
    assert_int_equal(invoke_query_func(query_obj, "getResult", NULL, NULL, NULL, NULL, NULL, &ret), 0);
    assert_true(amxc_var_type_of(&ret) == AMXC_VAR_ID_BOOL);
    assert_true(state == amxc_var_constcast(bool, &ret));

    // Add the given flag
    assert_int_equal(invoke_query_func(intf_obj, "setFlag", flag, "", "down", NULL, NULL, &ret), 0);

    // Get the query result
    assert_int_equal(invoke_query_func(query_obj, "getResult", NULL, NULL, NULL, NULL, NULL, &ret), 0);
    assert_true(amxc_var_type_of(&ret) == AMXC_VAR_ID_BOOL);
    assert_true(state != amxc_var_constcast(bool, &ret));

    // Remove the given flag
    assert_int_equal(invoke_query_func(intf_obj, "clearFlag", flag, "", "down", NULL, NULL, &ret), 0);

    // Close query
    assert_int_equal(invoke_query_func(intf_obj, "closeQuery", expr, NULL, "this", "me", "isUp", &ret), 0);

    amxc_var_clean(&ret);
}

void test_query_open(UNUSED void** state) {
    amxd_object_t* intf_obj = amxd_dm_findf(&dm, "NetModel.Intf.intf_0");
    amxc_var_t ret;
    amxc_var_t ret_cmp;

    assert_non_null(intf_obj);
    amxc_var_init(&ret);
    amxc_var_init(&ret_cmp);

    // Open query
    assert_int_equal(invoke_query_func(intf_obj, "openQuery", "red", "!blue", "down", "me", "hasFlag", &ret), 0);
    check_query_open(&ret, intf_obj);

    // Opening the same query multiple times should return the same id
    assert_int_equal(invoke_query_func(intf_obj, "openQuery", "red", "!blue", "down", "me", "hasFlag", &ret_cmp), 0);
    check_query_open(&ret_cmp, intf_obj);
    assert_int_equal(amxc_var_constcast(uint32_t, &ret), amxc_var_constcast(uint32_t, &ret_cmp));

    // Not all params are mandatory
    assert_int_equal(invoke_query_func(intf_obj, "openQuery", "red", NULL, NULL, "me", "hasFlag", &ret), 0);
    check_query_open(&ret, intf_obj);

    // Nonexistent class
    assert_int_not_equal(invoke_query_func(intf_obj, "openQuery", "red", "!blue", "down", "me", "nope", &ret), 0);

    // Missing mandatory params
    assert_int_not_equal(invoke_query_func(intf_obj, "openQuery", "red", "!blue", "down", NULL, "hasFlag", &ret), 0);
    assert_int_not_equal(invoke_query_func(intf_obj, "openQuery", NULL, "!blue", "down", "me", "hasFlag", &ret), 0);
    assert_int_not_equal(invoke_query_func(intf_obj, "openQuery", "red", "!blue", "down", "me", NULL, &ret), 0);
}

void test_query_close(UNUSED void** state) {
    amxd_object_t* intf_obj = amxd_dm_findf(&dm, "NetModel.Intf.intf_0");
    amxc_var_t ret;
    uint32_t id = 0;

    assert_non_null(intf_obj);
    amxc_var_init(&ret);

    // Open query
    assert_int_equal(invoke_query_func(intf_obj, "openQuery", "red", "!blue", "down", "me", "hasFlag", &ret), 0);
    check_query_open(&ret, intf_obj);
    id = amxc_var_constcast(uint32_t, &ret);

    // Close query
    assert_int_equal(invoke_query_func(intf_obj, "closeQuery", "red", "!blue", "down", "me", "hasFlag", &ret), 0);
    assert_null(amxd_object_findf(intf_obj, "Query.%d.", id));

    // Queries must be closed as many times as they are opened to be completely removed
    for(unsigned int i = 0; i < 10; i++) {
        assert_int_equal(invoke_query_func(intf_obj, "openQuery", "red", "!blue", "down", "me", "hasFlag", &ret), 0);
        check_query_open(&ret, intf_obj);
        id = amxc_var_constcast(uint32_t, &ret);
    }

    for(unsigned int i = 0; i < 9; i++) {
        assert_int_equal(invoke_query_func(intf_obj, "closeQuery", "red", "!blue", "down", "me", "hasFlag", &ret), 0);
        assert_non_null(amxd_object_findf(intf_obj, "Query.%d.", id));
    }
    assert_int_equal(invoke_query_func(intf_obj, "closeQuery", "red", "!blue", "down", "me", "hasFlag", &ret), 0);
    assert_null(amxd_object_findf(intf_obj, "Query.%d.", id));

    // Close nonexisting query
    assert_int_not_equal(invoke_query_func(intf_obj, "closeQuery", "nope", "!blue", "down", "me", "hasFlag", &ret), 0);
}

void test_query_trigger(UNUSED void** state) {
    amxd_object_t* intf_obj = amxd_dm_findf(&dm, "NetModel.Intf.intf_0");
    amxc_var_t ret;

    assert_non_null(intf_obj);
    amxc_var_init(&ret);

    // Add the "up" flag
    assert_int_equal(invoke_query_func(intf_obj, "setFlag", "up", "", "down", NULL, NULL, &ret), 0);

    // test with different conditions
    test_query_trigger_regexp("!blue", "blue", true);
    test_query_trigger_regexp("blue", "blue", false);
    test_query_trigger_regexp("red and blue", "red blue", false);
    test_query_trigger_regexp("not blue and red", "red", false);
    test_query_trigger_regexp("!(blue||!red)&&green", "red green", false);

    amxc_var_clean(&ret);
}

void test_query_dhcp_compare(UNUSED void** state) {
    amxd_object_t* intf_obj = amxd_dm_findf(&dm, "NetModel.Intf.intf_0");
    amxd_object_t* query_obj = NULL;
    amxc_var_t ret;
    uint32_t id = 0;
    amxc_set_t set;

    assert_non_null(intf_obj);
    amxc_var_init(&ret);
    amxc_set_init(&set, false);

    // Open query
    assert_int_equal(open_dhcp_query(intf_obj, "req", 1, "this", "me", &ret), 0);
    check_query_open(&ret, intf_obj);
    id = amxc_var_constcast(uint32_t, &ret);
    query_obj = amxd_object_findf(intf_obj, "Query.%d.", id);

    assert_non_null(query_obj);

    // open the same query, arguments will be compared
    amxc_set_parse(&set, "1 req");
    update_queries((intf_t*) intf_obj->priv, &set, REASON_DHCP);
    amxc_set_clean(&set);
    assert_int_equal(open_dhcp_query(intf_obj, "req", 1, "this", "me", &ret), 0);
    check_query_open(&ret, intf_obj);
    id = amxc_var_constcast(uint32_t, &ret);
    query_obj = amxd_object_findf(intf_obj, "Query.%d.", id);

    assert_non_null(query_obj);
}

void test_query_nonexistent_getFirstParameter_update(UNUSED void** state) {
    amxd_object_t* intf_obj = amxd_dm_findf(&dm, "NetModel.Intf.intf_0");
    amxd_object_t* query_obj = NULL;
    amxc_var_t ret;
    uint32_t id = 0;
    amxd_param_t* param = NULL;
    amxd_trans_t trans;

    assert_non_null(intf_obj);
    amxc_var_init(&ret);
    amxd_trans_init(&trans);

    // Open query for a nonexisting parameter
    assert_int_equal(open_getFirstParameter_query(intf_obj, "NetDevName", "this", "me", &ret), 0);
    check_query_open(&ret, intf_obj);
    id = amxc_var_constcast(uint32_t, &ret);
    query_obj = amxd_object_findf(intf_obj, "Query.%d.", id);
    assert_non_null(query_obj);

    // Resultstring must be empty
    assert_int_equal(strcmp("", amxc_var_constcast(cstring_t, amxd_object_get_param_value(query_obj, "ResultString"))), 0);

    // Simulate a mib being added
    // add NetDevName param to object with value blabla
    assert_int_equal(amxd_param_new(&param, "NetDevName", AMXC_VAR_ID_CSTRING), 0);
    assert_int_equal(amxd_param_set_attr(param, amxd_pattr_instance, true), 0);
    assert_int_equal(amxd_object_add_param(intf_obj, param), 0);
    amxd_trans_select_object(&trans, intf_obj);
    amxd_trans_set_value(cstring_t, &trans, "NetDevName", "blabla");
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    handle_events();

    // Query result should be blabla
    assert_int_equal(strcmp("blabla", amxc_var_constcast(cstring_t, amxd_object_get_param_value(query_obj, "ResultString"))), 0);

    amxd_trans_clean(&trans);
    amxc_var_clean(&ret);
}

void test_query_getFirstParameter_multiple_params_update(UNUSED void** state) {
    amxd_object_t* intf_obj = amxd_dm_findf(&dm, "NetModel.Intf.intf_0");
    amxd_object_t* query_obj = NULL;
    amxd_object_t* sub_obj = NULL;
    amxc_var_t ret;
    uint32_t id = 0;
    amxd_param_t* param = NULL;
    amxd_trans_t trans;

    assert_non_null(intf_obj);
    amxc_var_init(&ret);
    amxd_trans_init(&trans);

    // Add an object with 2 parameters to the intf
    assert_int_equal(amxd_object_new(&sub_obj, amxd_object_singleton, "SubObject"), 0);
    assert_int_equal(amxd_param_new(&param, "param_A", AMXC_VAR_ID_CSTRING), 0);
    assert_int_equal(amxd_param_set_attr(param, amxd_pattr_instance, true), 0);
    assert_int_equal(amxd_object_add_param(sub_obj, param), 0);
    assert_int_equal(amxd_param_new(&param, "param_B", AMXC_VAR_ID_CSTRING), 0);
    assert_int_equal(amxd_param_set_attr(param, amxd_pattr_instance, true), 0);
    assert_int_equal(amxd_object_add_param(sub_obj, param), 0);
    assert_int_equal(amxd_object_add_object(intf_obj, sub_obj), 0);

    // Open query for a parameter in a subobject of the netmodel intf
    assert_int_equal(open_getFirstParameter_query(intf_obj, "SubObject.param_A", "this", "me", &ret), 0);
    check_query_open(&ret, intf_obj);
    id = amxc_var_constcast(uint32_t, &ret);
    query_obj = amxd_object_findf(intf_obj, "Query.%d.", id);
    assert_non_null(query_obj);

    // Resultstring must be empty
    assert_int_equal(strcmp("", GET_CHAR(amxd_object_get_param_value(query_obj, "ResultString"), NULL)), 0);

    // Update both parameters
    amxd_trans_select_object(&trans, sub_obj);
    amxd_trans_set_value(cstring_t, &trans, "param_A", "This is A");
    amxd_trans_set_value(cstring_t, &trans, "param_B", "This is B");
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    handle_events();

    // Query result should be blabla
    assert_int_equal(strcmp("This is A", GET_CHAR(amxd_object_get_param_value(query_obj, "ResultString"), NULL)), 0);

    amxd_trans_clean(&trans);
    amxc_var_clean(&ret);
}

void test_query_getAddrs_with_delegate_update(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* intf_obj = amxd_dm_findf(&dm, "NetModel.Intf.1.");
    amxd_object_t* query_obj = NULL;
    amxc_var_t ret;
    amxc_var_t result_1;
    amxc_var_t result_2;
    int cmp = 0;

    assert_non_null(intf_obj);
    amxc_var_init(&ret);
    amxc_var_init(&result_1);
    amxc_var_init(&result_2);
    amxd_trans_init(&trans);

    // Create a new Intf with the test_ip mib
    amxd_trans_select_pathf(&trans, "NetModel.Intf.");
    amxd_trans_add_inst(&trans, 0, "intf_1");
    amxd_trans_set_value(cstring_t, &trans, "Flags", "test_ip");
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    handle_events();

    // Add test_ip mib to Intf 1
    assert_int_equal(invoke_query_func(intf_obj, "setFlag", "test_ip", "", "this", NULL, NULL, &ret), 0);
    handle_events();

    // Add an IPv6 address to Intf 1
    amxd_trans_clean(&trans);
    amxd_trans_select_pathf(&trans, "NetModel.Intf.1.IPv6Address.");
    amxd_trans_add_inst(&trans, 1, "");
    amxd_trans_set_value(cstring_t, &trans, "IPAddress", "1:abcd::1");
    // Set InterfacePath for Intf 1
    amxd_trans_select_pathf(&trans, "NetModel.Intf.1.");
    amxd_trans_set_value(cstring_t, &trans, "InterfacePath", "Device.IP.Interface.3.");
    // Set the addressDelegate for Intf 2
    amxd_trans_select_pathf(&trans, "NetModel.Intf.2.");
    amxd_trans_set_value(cstring_t, &trans, "IPv6AddressDelegate", "Device.IP.Interface.3.");
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    handle_events();

    // Open a query on the Intf with the addressDelegate
    intf_obj = amxd_dm_findf(&dm, "NetModel.Intf.2.");
    assert_non_null(intf_obj);
    assert_int_equal(invoke_query_func(intf_obj, "openQuery", "", "", "down", "me", "getAddrs", &ret), 0);
    check_query_open(&ret, intf_obj);

    // Store the current query result
    query_obj = amxd_object_findf(intf_obj, "Query.%d.", GET_UINT32(&ret, NULL));
    assert_non_null(query_obj);
    assert_int_equal(amxd_object_get_param(query_obj, "ResultString", &result_1), amxd_status_ok);

    // Update the IP address
    amxd_trans_clean(&trans);
    amxd_trans_select_pathf(&trans, "NetModel.Intf.1.IPv6Address.1.");
    amxd_trans_set_value(cstring_t, &trans, "IPAddress", "1:abcd::2");
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    handle_events();

    // Verify that the query has been updated
    assert_int_equal(amxd_object_get_param(query_obj, "ResultString", &result_2), amxd_status_ok);
    assert_int_equal(amxc_var_compare(&result_1, &result_2, &cmp), 0);
    assert_int_not_equal(cmp, 0);

    amxc_var_clean(&ret);
    amxc_var_clean(&result_1);
    amxc_var_clean(&result_2);
    amxd_trans_clean(&trans);
}
